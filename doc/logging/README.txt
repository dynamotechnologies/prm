DMDLog stores some basic information on data transactions.

Import this registry key to create DMDLog which can be viewed by running 'eventvwr' from the command prompt.  

The log will be found under 'Applications and Services Logs' named 'DMDLog'.