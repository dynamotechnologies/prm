To run the jmeter test on a server, do the following
1) install jmeter
2) copy the contents of /lib to {jmeterhome}/lib/ext
3) rename the config files from *.txt.default to *.txt (done this way so 
	that local changes don't get accidentally pushed to svn)
4) edit the config files
	a) global -- global config values.  You should be able to retarget the script
		and tweak the load without exiting the jmx file.
		* port -- port for the target webserver
		* protocol -- protocol to use (http or https)
		* host -- the name of the server 
		* api_prefix -- the portion of the api url up to "Main.svc"
		* users -- number of simultaneous users
		* repetitions -- number of times to run the script for each user.
	b) userdata -- there are two variables, username and projectid.  These correspond ot the 
		values you were enter into Default.aspx.  Each line is used for a "thread iteration" 
		(if you have 5 users and 2 repetitions you have 10 thread itertaions).  If there are 
		more iterations then entries they will be reused.  This isn't a problem per se, but if
		they cycle too fast you might end up with two users hitting the same project at the same 
		time which could be problematic (this could be true in the wild as well, but seems to 
		be vanishingly unlikely).  Recommend either having enough entries cover all iterations 
		or at least twice as many projects as users
5) open the jmeter test in the GUI or run from the command line
	a) jmeter -nt /some/path/code/PRM/doc/jmeter/prm.jmx
