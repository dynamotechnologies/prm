﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;

namespace DMD
{
    public class Container : IComparable<Container>
    {
        #region Declarations

        public long? ID { get; set; }            
        public int Order { get; private set; }
        public string Label { get; set; }
        public bool Static { get; set; }

        public Container Parent;

        public List<Document> Documents { get; private set; }
        public List<Container> Containers { get; private set; }

        #endregion

        #region Interfaces
        int IComparable<Container>.CompareTo(Container other)
        {
            //null is less than any value but equal to null
            if (ID == null)
            {
                return (other.ID == null ? 0 : -1);
            }
            
            if (other.ID == null)
            {
                return 1;
            }

            //otherwise we have two values, return the integer compare
            return ID.Value.CompareTo(other.ID.Value);
        }

        #endregion

        #region constructors

        public static Container CreateFromTemplateItem(Container initParent, TemplateItem item)
        {
            return new Container()
            {
                ID = null,
                Parent = initParent,
                Order = item.order,
                Label = item.label,
                Static = item.fixedFlag
            };
        }

        public static Container CreateFromContainer(Container initParent, Container container)
        {
            return new Container()
            {
                ID = container.ID,
                Parent = initParent,
                Order = container.Order,
                Label = container.Label,
                Static = container.Static
            };
        }


        public Container(long ID, Container Parent, int Order, string Label, bool Static)
        {
            this.ID = ID;
            this.Parent = Parent;
            this.Order = Order;
            this.Label = Label;
            this.Static = Static;

            Documents = new List<Document>();
            Containers = new List<Container>();
        }

        private Container()
        {
            Documents = new List<Document>();
            Containers = new List<Container>();
        }
        #endregion      

        #region Public members

        public void AddChild(Container child, long? afterid)
        {
            IEnumerable<Container> reorderList;

            //set the order value of child to make it after the selected node
            //if we don't have node to put it after, add it to the beginning of the list.
            if (afterid == null)
            {
                child.Order = 1;
                reorderList = Containers;
            }
            else
            {
                //we should always find something here, but if we don't then treat it as
                //if we deliberately added it to the beginning of the list.
                Container after = Containers.Find(c => c.ID == afterid);
                if (after == null)
                {
                    afterid = null;
                    child.Order = 1;
                    reorderList = Containers;
                }
                else
                {
                    child.Order = after.Order + 1;
                    reorderList = Containers.FindAll(c => c.Order > after.Order);
                }
            }

            //tweak the order of everybody else to match -- note that if we have
            //two items with the same order value in the reorder list we will not 
            //attempt to preserve this -- they will be explicitly ordered according 
            //to whatever 
            int mustClear = child.Order;
            foreach (Container reorder in reorderList.OrderBy(c => c.Order))
            {
                //child might already be in the list and its good.
                if(reorder.ID != child.ID)
                {
                    if (reorder.Order <= mustClear)
                    {
                        mustClear++;
                        reorder.Order = mustClear;
                    }
                }
            }

            child.Parent = this;
            //actually add the child
            if (!Containers.Contains(child))
            {
                Containers.Add(child);
            }
        }
        #endregion      
    }
}
