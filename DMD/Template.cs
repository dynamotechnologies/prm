﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace DMD
{
    public class TemplateHeaderPage
    {
        public List<TemplateHeader> templates { get; protected set; }
        public string[] unitids { get; protected set; }
        public int page { get; protected set; }
        public int pagesize { get; protected set; }

        public int maxpage { get; protected set; }

        public static TemplateHeaderPage load(int pagenum, int pagesize)
        {
            return load(Enumerable.Empty<string>().ToArray(), pagenum, pagesize);
        }

        public static TemplateHeaderPage load(string[] unitids, int pagenum, int pagesize)
        {
            TemplateHeaderPage page = new TemplateHeaderPage(pagenum, pagesize)
            {
                unitids = unitids,
            };

            //start is zero based, which makes things easier to calculate)
            int start = (page.page - 1) * page.pagesize;
            
            StringBuilder sb = new StringBuilder();
            sb.Append("containertemplates/headers?");
            sb.Append("start=").Append(start).Append("&rows=").Append(pagesize);
            
            if (unitids.Length > 0)
            {
                sb.Append("&");
                foreach (String unitid in unitids)
                {
                    sb.Append("unitid=").Append(unitid).Append("&");
                }
                sb.Length -= "&".Length;
            }

            string xml = WADL.Get(sb.ToString());

            XmlDocument doc = new XmlDocument();
            doc.Load(new StringReader(xml));

            page.templates = TemplateHeader.getHeaders(doc);
            page.setPageFields(doc);
            return page;
        }

        private void setPageFields(XmlDocument doc)
        {
            XmlNode parentNode = doc.DocumentElement;
            foreach (XmlNode child in parentNode.ChildNodes)
            {
                if (child.Name == "total")
                {
                    int total = XmlConvert.ToInt32(child.InnerText);
                    maxpage = (int) Math.Ceiling(total / (float)pagesize);
                }
            }
        }

        private TemplateHeaderPage(int page, int pagesize)
        {
            if (pagesize > 100)
            {
                pagesize = 100;
            }

            if (pagesize <= 0)
            {
                pagesize = 15;
            }

            if (page <= 0)
            {
                page = 1;
            }

            this.page = page;
            this.pagesize = pagesize;
        }
    }

    public class TemplateHeader
    {
        public long? templateid { get; protected set; }
        public string unitid { get; protected set; }
        public string name { get; protected set; }
        public DateTime? created { get; protected set; }

        public static List<TemplateHeader> getHeaders(XmlDocument doc)
        {
            List<TemplateHeader> headers = new List<TemplateHeader>();
            XmlNode parentNode = doc.DocumentElement;
            foreach (XmlNode child in parentNode.ChildNodes)
            {
                if (child.Name == "container")
                {
                    headers.Add(createTemplateHeaderFromNode(child));
                }
            }

            return headers;
        }

        private static TemplateHeader createTemplateHeaderFromNode(XmlNode node)
        {
            TemplateHeader header = new TemplateHeader();
            header.setHeaderFields(node);
            return header;
        }

        protected void setHeaderFields(XmlNode node)
        {
            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name == "templateid")
                {
                    this.templateid = XmlConvert.ToInt64(child.InnerText);
                }
                else if (child.Name == "unitid")
                {
                    this.unitid = child.InnerText;
                }
                else if (child.Name == "name")
                {
                    this.name = child.InnerText;
                }
                else if (child.Name == "created")
                {
                    DateTime date;
                    if (DateTime.TryParse(child.InnerText, out date))
                    {
                        created = date;
                    }
                    else
                    {
                        created = null;
                    }
                }

            }
        }

        protected TemplateHeader()
        {
        }
    }

    public class Template : TemplateHeader
    {
        public List<TemplateItem> items { get; private set; }

        public static Template CreateFromProject(Project project, string name, string unitid)
        {
            Template template = new Template();
            template.SetItems(project);

            template.templateid = null;
            template.unitid = unitid;
            template.name = name;

            return template;
        }

        public static Template Load(int templateid)
        {
            Template t = new Template();

            string xml = WADL.Get("containertemplates/" + templateid);

            XmlDocument doc = new XmlDocument();
            doc.Load(new StringReader(xml));

            XmlNode templateNode = doc.DocumentElement;

            t.setHeaderFields(templateNode);
            foreach (XmlNode child in templateNode.ChildNodes)
            {
                if (child.Name == "containers")
                {
                    t.SetItems((XmlElement)child);
                }
            }

            return t;
        }

        public static void Delete(int templateid)
        {
            WADL.Delete("containertemplates/" + templateid);
        }

        private Template()
        {
            items = new List<TemplateItem>();
        }

        private void SetItems(Project project)
        {
            foreach (Container container in project.Containers.Items)
            {
                items.Add(TemplateItem.CreateFromContainer(container));
            }
        }

        private void SetItems(XmlElement containers)
        {
            foreach (XmlNode child in containers.ChildNodes)
            {
                if (child.Name == "container")
                {               
                    items.Add(TemplateItem.CreateFromXml((XmlElement)child));
                }
            }
        }

        public void Commit()
        {
            Encoding encoding = new UTF8Encoding(false);
            byte[] data = GetCreateXml(encoding);

            WADL.Upload(Method.POST, "containertemplates", data, encoding);
        }

        public bool hasIDs()
        {
            if (templateid == null)
            {
                return false;
            }

            foreach (TemplateItem item in items)
            {
                if (!item.hasIDs())
                {
                    return false;
                }
            }

            return true;
        }

        private byte[] GetCreateXml(Encoding encoding)
        {
            using (MemoryStream output = new MemoryStream())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = encoding;

                XmlWriter xml = XmlWriter.Create(output, settings);

                xml.WriteStartDocument();
                xml.WriteStartElement("containertemplate", "http://www.fs.fed.us/nepa/schema");

                if (templateid != null)
                {
                    xml.WriteElementString("templateid", XmlConvert.ToString(templateid.Value));
                }
                xml.WriteElementString("name", name);
                xml.WriteElementString("unitid", unitid);

                xml.WriteStartElement("containers");
                foreach (TemplateItem item in items)
                {
                    AddItemXml(xml, item);
                }

                xml.WriteEndElement();
                xml.WriteEndElement();
                xml.WriteEndDocument();
                xml.Close();

                return output.ToArray();
            }
        }

        private void AddItemXml(XmlWriter xml, TemplateItem item)
        {
            xml.WriteStartElement("container");

            if (item.itemid != null)
            {
                xml.WriteAttributeString("itemid", XmlConvert.ToString(item.itemid.Value));
            }

            xml.WriteAttributeString("fixedflag", XmlConvert.ToString(item.fixedFlag));
            xml.WriteAttributeString("palscontainerid", XmlConvert.ToString(item.palsContainerId));
            xml.WriteAttributeString("label", item.label);
            xml.WriteAttributeString("order", XmlConvert.ToString(item.order));

            foreach (TemplateItem child in item.items)
            {
                AddItemXml(xml, child);
            }
            xml.WriteEndElement();
        }
    }
}
