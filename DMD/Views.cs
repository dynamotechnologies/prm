﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Globalization;
using System.Security;



namespace DMD
{
    public class ViewsHeader
    {
        public string viewid { get; protected set; }
        public string viewname { get; protected set; }

        public static List<ViewsHeader> getViewHeaders(Project projectid, string userid)
        {
            
            List<ViewsHeader> viewheaders = new List<ViewsHeader>();
            string xml;
            xml = WADL.Get(Configuration.Projects + projectid.ID + "/views");

            //var xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><projectViews xmlns=\"\"><projectView><id>2</id><name>test</name><projectId >46776</projectId ><userId >oshroff</userId><createDate>07/12/2016</createDate></projectView><projectView><id>3</id><name>test1</name><projectId >42030</projectId ><userId >oshroff</userId><createDate>12/12/2016</createDate></projectView></projectViews>";


            XmlDocument doc = new XmlDocument();
            doc.Load(new StringReader(xml));

            XmlNode parentNode = doc.DocumentElement;
            foreach (XmlNode child in parentNode.ChildNodes)
            {
                if (child.Name == "projectView")
                {
                    viewheaders.Add(createViewHeaderFromNode(child));
                }
               
            }

            return viewheaders;
        }

        private static ViewsHeader createViewHeaderFromNode(XmlNode node)
        {
            ViewsHeader viewheader = new ViewsHeader();

            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name == "id")
                {
                    viewheader.viewid = child.InnerText;
                }
                else if (child.Name == "name")
                {
                    viewheader.viewname = child.InnerText;
                }
            }
            return viewheader;
        }

        protected ViewsHeader()
        {
        }

        public static void createView(Project project, string userid, string name)
        {
            try
            {
                string sCreatetDate;                

                sCreatetDate = DateTime.Now.ToString("yyyy-MM-dd");

                name = SecurityElement.Escape(name);

                var sInputXML = "<projectView xmlns=\"http://www.fs.fed.us/nepa/schema\"><name>" + name +
                                "</name><projectId>" + project.ID.ToString() + "</projectId><userId>" + userid +
                                "</userId><createDate>" + sCreatetDate + "</createDate></projectView>";

                WADL.Upload(Method.POST, Configuration.Projects + project.ID + "/views", sInputXML);

                //string response = WADL.Upload(Method.POST, "views/save", data);
               
            }

            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void deleteView(Project project , string viewid)
        {
            WADL.Delete(Configuration.Projects + project.ID + "/views/" + viewid);
        }


        public static void addToView(Project project,string node_id,string viewid)
        {

            var sInputXML = "<projectViewDoc xmlns=\"http://www.fs.fed.us/nepa/schema\"><viewId>" + viewid +
                             "</viewId><documentId>" + node_id + " </documentId></projectViewDoc>";

            WADL.Upload(Method.POST, Configuration.Projects + project.ID + "/views/" + viewid + "/docs", sInputXML);

            //string response = WADL.Upload(Method.POST, "views/add", data);
            
        }        
       
    }
}
