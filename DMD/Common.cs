﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Data;
using System.Diagnostics;

namespace DMD
{
    public enum ProjectInfo
    {
        Header,
        Details,
        Full
    }

    public enum DocumentSortOrder
    {
        Alphabetical,
        DateAdded
    }

    public enum Method
    {
        PUT,
        POST,
        DELETE
    }

    public enum DocumentGridLists
    {
        SensitivityRationales,
        DateLabels
    }

    public static class Configuration
    {
        public static string Projects = "projects/nepa/";
        public static string Users = "users/";
        public static string UserSessions = "usersessions/";

        public static string TokenPassword;

        public static string Connection;
        public static string BrokerConnection;
        public static string DSRConnection;
        public static NetworkCredential ConnectionCredentials; 
        public static NetworkCredential BrokerConnectionCredentials; 
        public static NetworkCredential DSRConnectionCredentials; 

        public static void init(System.Configuration.ApplicationSettingsBase settings)
        {
            try
            {
                TokenPassword = settings["TokenPassword"].ToString();

                Connection = settings["Connection"].ToString();
                ConnectionCredentials = new NetworkCredential(settings["UserName"].ToString(), settings["Password"].ToString());

                BrokerConnection = settings["BrokerConnection"].ToString();
                BrokerConnectionCredentials = new NetworkCredential(settings["BrokerUserName"].ToString(), settings["BrokerPassword"].ToString());

                DSRConnection = settings["DSRConnection"].ToString();
                DSRConnectionCredentials = new NetworkCredential(settings["DSRUserName"].ToString(), settings["DSRPassword"].ToString());
            }
            catch (Exception e)
            {
                General.WriteToEventLog(e.Message, true);
                throw;
            }
        }
    }

    public static class General
    {
        public static Dictionary<int, string> GetSensitivityRationales()
        {
            Dictionary<int, string> oReturn = new Dictionary<int,string>();

            try
            {
	            DataSet oDSRSet = new DataSet();

                var sXML = WADL.GetDSR();
                StringReader oReader = new StringReader(sXML);
                oDSRSet.ReadXml(oReader);

                foreach (DataRow oRow in oDSRSet.Tables[0].Rows)
                {
                    oReturn.Add(Convert.ToInt16(oRow["id"].ToString()),oRow["description"].ToString());
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return oReturn;
        }

        public static Dictionary<int, string> GetDateLabels()
        {
            Dictionary<int, string> oReturn = new Dictionary<int, string>();

            try
            {
               oReturn.Add(0, "Signed On");
               oReturn.Add(1, "Published On");
               oReturn.Add(2, "Delivered On");
               oReturn.Add(3, "Completed On");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return oReturn;
        }

        public static void WriteToEventLog(string Message, bool Error)
        {
            EventLog eventLog = null;
            try
            {
                if (!EventLog.SourceExists("DMDLog"))
                {
                    EventLog.CreateEventSource("DMDLog", "DMDLog");
                }
                eventLog = new EventLog { Source = "DMDLog" };
                eventLog.WriteEntry(Message, Error ? EventLogEntryType.Error : EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                try
                {
                    //if we bailed trying to create the event log, this isn't going to work.
                    if (eventLog != null)
                    {
                        eventLog.WriteEntry("Failed to log message: " + ex.Message, EventLogEntryType.Error);
                    }
                }
                catch (Exception)
                {
                    // we really, really tried.
                }
            }
        }
    }

    public static class WADL
    {
        public static string Get(string Resource)
        {
            string sReturn;

            try
            {
                //Create request object
                var oRequest = (HttpWebRequest)WebRequest.Create(Configuration.Connection + Resource);
                oRequest.Credentials = Configuration.ConnectionCredentials;

                //Get the response using the request
                HttpWebResponse oResponse = oRequest.GetResponse() as HttpWebResponse;

                //Read the stream from the response object
                Stream oStream = oResponse.GetResponseStream();
                StreamReader oStreamReader = new StreamReader(oStream);

                // Read the result from the stream reader
                sReturn = oStreamReader.ReadToEnd();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return sReturn;
        }

        public static string GetDSR()
        {
            string sReturn;

            try
            {
                //Create request object
                var oRequest = (HttpWebRequest)WebRequest.Create(Configuration.DSRConnection);
                oRequest.Credentials = Configuration.DSRConnectionCredentials;

                //Get the response using the request
                HttpWebResponse oResponse = oRequest.GetResponse() as HttpWebResponse;

                //Read the stream from the response object
                Stream oStream = oResponse.GetResponseStream();
                StreamReader oStreamReader = new StreamReader(oStream);

                // Read the result from the stream reader
                sReturn = oStreamReader.ReadToEnd();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return sReturn;
        }

        public static string Delete(string Resource)
        {
            //should seperate this eventually, its a little awkward.
            return Upload(Method.DELETE, Resource, null, null);
        }

        public static string Upload(Method Method, string Resource, string Data = "")
        {
            ASCIIEncoding oEncode = new ASCIIEncoding();

            if (Data != "")
            {
                return Upload(Method, Resource, oEncode.GetBytes(Data), oEncode);
            }
            else
            {
                return Upload(Method, Resource, null, null);
            }
        }

        public static string Upload(Method Method, string Resource, byte[] Data, Encoding oEncode)
        {
            byte[] oResponse;
            string sResponse;

            String sData = "";
            if (Data != null)
            {
                sData = oEncode.GetString(Data);
            }

            string url = Configuration.Connection + Resource;
            General.WriteToEventLog("Request -> " + url + "(" + Method.ToString() + "): " + sData, false);
            using (var oClient = new WebClient())
            {
                oClient.Credentials = Configuration.ConnectionCredentials;
                if (Method == Method.DELETE)
                {
                    sResponse = oClient.UploadString(url, "DELETE", "");
                }
                else
                {
                    oResponse = oClient.UploadData(url, Method == Method.PUT ? "PUT" : "POST", Data);
                    sResponse = new string(oEncode.GetChars(oResponse));                        
                }                    
            }

            if (sResponse.Contains("<title>Error</title>"))
            {
                General.WriteToEventLog("Response -> " + sResponse, true);
                throw new Exception("Unhandled error occurred. Please, contact system administrator for assist.");
            }
            else
            {
                General.WriteToEventLog("Response -> " + sResponse, false);
            }

            return sResponse;
        }
    }
}
