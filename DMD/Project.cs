﻿using System;
using System.Data;
using System.IO;
using System.Xml;

namespace DMD
{
    public class Project
    {
        #region Declarations

        ProjectInfo loadlevel;

        private int iID;
        private string sName;
        private string sUserID;
        private string sUnitCode;       //TODO in future version - compare this with User's Unit level roles
        private string sProjectDocumentID;  //This is a PALS specific ID that is used to construct the publication location
        private DateTime? dLastUpdate;
        private Documents oDocuments;
        private Containers oContainers;
        

        public static string LoadView;
       
        public int ID
        {
            get { return iID; }
        }

        public string Name
        {
            get { return sName; }
        }

        public string UnitCode
        {
            get { return sUnitCode; }
        }

        public string UserID
        {
            get { return sUserID; }
        }

        public string ProjectDocumentID
        {
            get { return sProjectDocumentID; }
        }

        public DateTime? LastUpdate
        {
            get { return dLastUpdate; }
        }

        public Containers Containers
        {
            get { return oContainers; }
        }

        public Documents Documents
        {
            get { return oDocuments; }
        }

        #endregion

        #region Private members

        public void GetProjectInfo()
        {
            try
            {
                string sXML;

                DateTime dDate;
                DataSet oDocumentSet = new DataSet();

                sXML = WADL.Get(Configuration.Projects + ID);
                StringReader oReader = new StringReader(sXML);

                XmlDocument oDoc = new XmlDocument();
                oDoc.Load(oReader);

                XmlNode oParentNode = oDoc.ChildNodes[1];

                foreach (XmlNode oChildNode in oParentNode.ChildNodes)
                {
                    if (oChildNode.Name == "name")
                    {
                        sName = oChildNode.InnerText;
                    }
                    if (oChildNode.Name == "lastupdate")
                    {
                        if (DateTime.TryParse(oChildNode.InnerText, out dDate))
                        {
                            dLastUpdate = dDate;
                        }
                        else
                        {
                            dLastUpdate = null;
                        }
                    }
                    if (oChildNode.Name == "unitcode")
                    {
                        sUnitCode = oChildNode.InnerText;
                    }
                    if (oChildNode.Name == "nepainfo")
                    {
                        foreach (XmlNode oNepaInfoNode in oChildNode.ChildNodes)
                        {
                            if (oNepaInfoNode.Name == "projectdocumentid")
                            {
                                sProjectDocumentID = oNepaInfoNode.InnerText;
                                break;
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        #endregion

        #region Public members

        public Project()
        {
        }

        public Project(int ID, ProjectInfo Load = ProjectInfo.Details, bool IncludeFileMetaData = false, string UserID = "")
        {
            loadlevel = Load;

            iID = ID;
            sUserID = UserID;
            if (Load == ProjectInfo.Header || Load == ProjectInfo.Full) { GetProjectInfo(); }
            if (Load == ProjectInfo.Details || Load == ProjectInfo.Full)
            {
                oDocuments = new Documents(ID, sProjectDocumentID, IncludeFileMetaData);
                oContainers = new Containers(ID, oDocuments);

                
            }
        }

        public Project(int ID, string loadview, ProjectInfo Load = ProjectInfo.Details, bool IncludeFileMetaData = false, string UserID = "")
        {
            loadlevel = Load;

            iID = ID;
            sUserID = UserID;
            if (Load == ProjectInfo.Header || Load == ProjectInfo.Full) { GetProjectInfo(); }
            if (Load == ProjectInfo.Details || Load == ProjectInfo.Full)
            {
                oDocuments = new Documents(ID, sProjectDocumentID, IncludeFileMetaData);
                oContainers = new Containers(ID, loadview, oDocuments);


            }
        }

        public void applyTemplate(Template template)
        {
            if (!(loadlevel == ProjectInfo.Full || loadlevel == ProjectInfo.Details))
            {
                throw new InvalidOperationException("Project details must loaded to apply templates");
            }

            Containers newContainers = Containers.CreateFromTemplate(this, template);
            this.oContainers = newContainers;

            //template create can change document array.  Need to sort out how to seperate this better.
            this.Documents.Commit();
            this.oContainers.Commit();
        }

        #endregion
    } 
}
