﻿using System;
using System.Data;
using System.IO;

namespace DMD
{
    public class Document : IComparable<Document>
    {
        #region Interfaces
        int IComparable<Document>.CompareTo(Document other)
        {
	        if (other.Order > Order)
                return -1;
	        return other.Order == Order ? 0 : 1;
        }

	    #endregion

        #region Declarations

        private bool bPublic;

        private bool bPublish;

        private bool bLoaded;

	    public string ProjectType { get; private set; }

	    public int ProjectID { get; private set; }

        public string ProjectDocumentID { get; private set; }

	    public long? ParentID { get; set; }

	    public int Order { get; set; }

	    public string ID { get; private set; }

	    public string Name { get; set; }

	    public string Description { get; set; }

	    public string DownloadOriginal { get; private set; }

	    public string DownloadPDF { get; private set; }

	    public Int32 PDFfileSize { get; private set; }

        public string Link { get; private set; }

	    public bool Public 
        {
            get { return bPublic; } 
            set 
                {
                    if (bLoaded && !bPublic && value)
                    {
                        bPublish = true;
                    }
                    bPublic = value; 
                }
        }

        public bool AllowPublish { get; set; }

	    public string Author { get; set; }

	    public string Addressee { get; set; }

	    public string DateLabel { get; set; }

	    public DateTime? Date { get; set; }
        //Added to update each field in UCM
        public string Currentrow { get; set; }

        public string Currentcolumn { get; set; }

        public string Previousvalue { get; set; }

        public string Currentvalue { get; set; }       

	    public DateTime? DateAdded { get; set; }

	    public string AddedBy { get; set; }

	    public int SensitivityRationale { get; set; }

        public int ContID { get; set; }

	    public FileMetaData FileMetaData { get; private set; }

	    #endregion       

        #region Public Members

        public Document()
        { 
        }

        public Document(string ID, string Name, string Description, long? ParentID, int ProjectID,
                        string ProjectType, string PDFfileSize, string Author, string Addressee, string DateLabel,
                        DateTime? Date, string AddedBy, DateTime? DateAdded, bool Public, int SensitivityRationale,int ContID, 
                        bool IncludeFileMetaData, string Link, string ProjectDocumentID = "")
        {
            
            this.ID = ID;
            this.ParentID = ParentID;
            this.ProjectID = ProjectID;
            this.ProjectDocumentID = ProjectDocumentID;
            this.ProjectType = ProjectType;
            this.Public = Public;
            this.DownloadOriginal = "DOWNLOAD," + ProjectID.ToString() + "," + ID; 
            this.DownloadPDF = "DOWNLOAD,PDF," + ProjectID.ToString() + "," + ID;
            this.Name = Name;
            this.Description = Description;
            this.PDFfileSize = GetFileSize(PDFfileSize);
            this.Author = Author;
            this.Addressee = Addressee;
            this.DateLabel = DateLabel;
            this.Date = Date;
            this.DateAdded = DateAdded;
            this.AddedBy = AddedBy;
            this.SensitivityRationale = SensitivityRationale;
            this.ContID = ContID;
            this.Link = Link;
            FileMetaData = IncludeFileMetaData ? new FileMetaData(ID) : new FileMetaData();
            bLoaded = true;
        }

        public Document(string ID, string ProjectID, string ProjectDocumentID = "")
        {
            try
            {
	            DateTime dDocumentDate;
                DateTime dDateAdded;
                DataSet oDocuments = new DataSet();
                DataTable oDocument = new DataTable();

                var sXML = WADL.Get(Configuration.Projects + ProjectID + "/docs" + (ID.Length > 0 ? "/" + ID : ""));
                StringReader oReader = new StringReader(sXML);
                oDocuments.ReadXml(oReader);

                if (oDocuments.Tables.Count > 0)
                {
                    oDocument = oDocuments.Tables[0];
                }

                this.ProjectDocumentID = ProjectDocumentID;
                if (oDocument.Rows.Count > 0)
                {
                    this.ID = oDocument.Rows[0]["docid"].ToString();
                    this.ProjectID = Convert.ToInt32(oDocument.Rows[0]["projectid"]);
                    ProjectType = oDocument.Rows[0]["projecttype"].ToString();
                    Public = Convert.ToBoolean(Convert.ToInt32(oDocument.Rows[0]["pubflag"].ToString()));
                    DownloadOriginal = "DOWNLOAD," + this.ProjectID.ToString() + "," + this.ID;
                    DownloadPDF = "DOWNLOAD,PDF," + this.ProjectID.ToString() + "," + this.ID;
                    Name = oDocument.Rows[0]["docname"].ToString();
                    Description = oDocument.Rows[0]["description"].ToString();
                    PDFfileSize = GetFileSize(oDocument.Rows[0]["pdffilesize"].ToString());
                    Link = oDocument.Rows[0]["wwwlink"].ToString();

                    if (oDocument.Columns.Contains("author")) { Author = oDocument.Rows[0]["author"].ToString(); }
                    if (oDocument.Columns.Contains("addressee")) { Addressee = oDocument.Rows[0]["addressee"].ToString(); }
                    if (oDocument.Columns.Contains("datelabel")) { DateLabel = oDocument.Rows[0]["datelabel"].ToString(); }
                    if (oDocument.Columns.Contains("addedby")) { AddedBy = oDocument.Rows[0]["addedby"].ToString(); }

                    if (oDocument.Columns.Contains("docdate"))
                    {
                        if (DateTime.TryParse(oDocument.Rows[0]["docdate"].ToString(), out dDocumentDate))
                        {
                            Date = dDocumentDate;
                        }
                        else
                        {
                            Date = null;
                        }
                    }
                    else
                    {
                        Date = null;
                    }

                    if (oDocument.Columns.Contains("dateadded"))
                    {
                        if (DateTime.TryParse(oDocument.Rows[0]["dateadded"].ToString(), out dDateAdded))
                        {
                            DateAdded = dDateAdded;
                        }
                        else
                        {
                            DateAdded = null;
                        }
                    }
                    else
                    {
                        DateAdded = null;
                    }

                    if (oDocument.Columns.Contains("sensitivityrationale"))
                    {
	                    int iOut;
	                    SensitivityRationale = int.TryParse(oDocument.Rows[0]["sensitivityrationale"].ToString(), out iOut) ? iOut : 0;
                    }
                    else
                    {
                        SensitivityRationale = 0;
                    }

                    if (oDocument.Columns.Contains("contid"))
                    {
                        int iOutContID;
                        ContID = int.TryParse(oDocument.Rows[0]["contid"].ToString(), out iOutContID) ? iOutContID : 0;
                    }
                    else
                    {
                        ContID = 0;
                    }
                }

                FileMetaData = new FileMetaData(ID);
                bLoaded = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        // Publish All update
        public void Update()
        {

            try
            {
                //Check if file exists
                if (bPublish)
                {
                    byte[] oContent = GetContent(true);
                    if (oContent.Length == 1)
                    {
                        throw new Exception("Document is not yet ready. Please try to publish after sometime");
                    }
                }

	            string sDocumentDate = "";
                if (Date != null)
                {                   
                    sDocumentDate = Date.Value.ToString("MM/dd/yyyy");                   
                    
                }                

                var sInputXML = "<projectdocument xmlns=\"http://www.fs.fed.us/nepa/schema\"><docid>" + ID +
                                   "</docid><projectid>" + ProjectID.ToString() + "</projectid><projecttype>" + ProjectType +
                                   "</projecttype><pubflag>" + (Public ? "1" : "0") + "</pubflag><wwwlink>" + Link + "</wwwlink><docname><![CDATA[" + Name +
                                   "]]></docname><description><![CDATA[" + Description + "]]></description><pdffilesize>" + PDFfileSize.ToString() + "kb</pdffilesize><author><![CDATA[" + Author +
                                   "]]></author><addressee><![CDATA[" + Addressee + "]]></addressee><addedby>" + AddedBy + "</addedby><datelabel>" + DateLabel +
                                   "</datelabel>" + (sDocumentDate.Trim().Length == 0 ? "" : "<docdate>" + sDocumentDate + "</docdate>") + 
                                   "<sensitivityrationale>" + SensitivityRationale.ToString() + "</sensitivityrationale></projectdocument>";
                
                //Datamart Update
                WADL.Upload(Method.PUT, Configuration.Projects + ProjectID + "/docs/" + ID, sInputXML);

                //*Commented to fix date format error
                if (Date != null)
                {
                    sDocumentDate = Date.Value.ToString("yyyy-MM-dd") + "T00:00:00Z";
                }

                //UCM update
                FileMetaData.Update(ID, Name, Description, Author, Addressee, DateLabel, sDocumentDate, AddedBy, SensitivityRationale);
                
                if (bPublish)
                {
                    XMLRPC oXMLPRC = new XMLRPC();
                    oXMLPRC.PublishFile(ID, ProjectDocumentID + "_" + ID + ".pdf");
                }
                                
            }
            catch (Exception e)
            {
                if (e.Message.Contains("Unable to download"))
                {
                    throw new Exception("Document is not yet ready. Please try to publish after sometime");
                }
                else
                {
                    throw new Exception(e.Message);
                }
            }
        }

        //Added to update each field in UCM
        public void UpdateField()
        {

            try
            {
                //Check if file exists
                if (bPublish)
                {
                    byte[] oContent = GetContent(true);
                    if (oContent.Length == 1)
                    {                       
                        throw new Exception("Document is not yet ready. Please try to publish after sometime");
                    }
                }

                string sDocumentDate = "";
                if (Date != null)
                {
                    sDocumentDate = Date.Value.ToString("MM/dd/yyyy");                  

                }

                var sInputXML = "<projectdocument xmlns=\"http://www.fs.fed.us/nepa/schema\"><docid>" + ID +
                                   "</docid><projectid>" + ProjectID.ToString() + "</projectid><projecttype>" + ProjectType +
                                   "</projecttype><pubflag>" + (Public ? "1" : "0") + "</pubflag><wwwlink>" + Link + "</wwwlink><docname><![CDATA[" + Name +
                                   "]]></docname><description><![CDATA[" + Description + "]]></description><pdffilesize>" + PDFfileSize.ToString() + "kb</pdffilesize><author><![CDATA[" + Author +
                                   "]]></author><addressee><![CDATA[" + Addressee + "]]></addressee><addedby>" + AddedBy + "</addedby><datelabel>" + DateLabel +
                                   "</datelabel>" + (sDocumentDate.Trim().Length == 0 ? "" : "<docdate>" + sDocumentDate + "</docdate>") +
                                   "<sensitivityrationale>" + SensitivityRationale.ToString() + "</sensitivityrationale></projectdocument>";

                //Datamart update
                WADL.Upload(Method.PUT, Configuration.Projects + ProjectID + "/docs/" + ID, sInputXML);

                //*Commented to fix date format error
                if (Date != null)
                {
                    sDocumentDate = Date.Value.ToString("yyyy-MM-dd") + "T00:00:00Z";
                }

                //UCM Update
                FileMetaData.UpdateField(ID, Name, Description, Author, Addressee, DateLabel, sDocumentDate, AddedBy, SensitivityRationale, Currentrow, Currentcolumn, Previousvalue, Currentvalue);

                if (bPublish)
                {
                    XMLRPC oXMLPRC = new XMLRPC();
                    oXMLPRC.PublishFile(ID, ProjectDocumentID + "_" + ID + ".pdf");
                }

            }
            catch (Exception e)
            {

                if (e.Message.Contains("Unable to download"))
                {
                    throw new Exception("Document is not yet ready. Please try to publish after sometime");
                }
                else
                {
                    throw new Exception(e.Message);
                }
                
            }
        }

        public bool Delete()
        {
	        try
            {
                WADL.Upload(Method.DELETE, Configuration.Projects + ProjectID + "/docs/" + ID);
                XMLRPC oXMLPRC = new XMLRPC();
                //Added to delete file from UCM as well
                oXMLPRC.DeleteFile(ID);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return true;
        }

        public byte[] GetContent(bool PDF = false)
        {
            XMLRPC oXMLRPC = new XMLRPC();
            return oXMLRPC.GetFileContent(ID, PDF ? "PDF" : "");
        }

        private Int32 GetFileSize(string FileSize)
        {
            Int32 iFileSize;
            Int32.TryParse(FileSize.Replace("kb",""), out iFileSize);
            return iFileSize;
        }

        #endregion      
    }
}
