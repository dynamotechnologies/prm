﻿using System;
using CookComputing.XmlRpc;

namespace DMD
{
    public interface IDMDXmlRpc : IXmlRpcProxy
    {
        [XmlRpcMethod("getFileMetadata")]
        FileMetaData GetFileMetaData(string DocID);

        [XmlRpcMethod("replaceFileMetadata")]
        bool ReplaceFileMetaData(string DocID, FileMetaData Document, FileMetaDataUpdateOptions Options);

        [XmlRpcMethod("getFileData")]
        byte[] GetFileData(string DocID, FileDataOptions Options);

        [XmlRpcMethod("getFileData")]
        byte[] GetFileData(string DocID);

        [XmlRpcMethod("deleteFile")]
        bool deleteFile(string DocID);

        [XmlRpcMethod("publishFile")]
        bool PublishFile(string Agent, string DocID, PublishFileOptions Options);
    }

    [Serializable]
    public struct MoreMetaData
    {
        public string projectid;
        public string docdate;
        //public string datelabel; // changed and misspelled
        //public string caraphaseid;
        //public string caraid;
        public string docsummary;
        public string docauthor;
        public string addressee;
        public string datelabel;
        public string sensitiveflag;
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string sensitiverat;

    }

    public struct FileMetaDataUpdateOptions
    {
        public bool mergeMM;
    }

    public struct FileDataOptions
    {
        public string fileType;
    }

    public struct PublishFileOptions
    {
        public string targetdir;
        public string targetname;
    }

    [Serializable]
    public class XMLRPC
    {
        IDMDXmlRpc oProxy;

        private IDMDXmlRpc Proxy
        {
            get 
            {
                if (oProxy == null)
                {
                    oProxy = XmlRpcProxyGen.Create<IDMDXmlRpc>();
                    oProxy.Url = Configuration.BrokerConnection;
                    oProxy.Credentials = Configuration.BrokerConnectionCredentials;
                }

                return oProxy;
            }            
        }

        public FileMetaData GetFileMetaData(string DocID)
        {
            FileMetaData oReturn;
            try
            {
                oReturn = Proxy.GetFileMetaData(DocID);
            }            
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return oReturn;
        }

        public bool ReplaceFileMetaData(string DocID, FileMetaData Document, FileMetaDataUpdateOptions Options)
        {
            bool oReturn;
            try
            {
                oReturn = Proxy.ReplaceFileMetaData(DocID, Document, Options);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return oReturn;
        }

        public bool PublishFile(string DocID, string TargetName)
        {
            bool oReturn = false;

            try
            {
                PublishFileOptions oOptions;
                oOptions.targetname = TargetName;
                oOptions.targetdir = "";

                oReturn = Proxy.PublishFile("PALS_WWW_1", DocID, oOptions);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return oReturn;
        }

        public bool DeleteFile(string DocID)
        {
            bool oReturn = false;

            try
            {

                oReturn = Proxy.deleteFile(DocID);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return oReturn;
        }

        public byte[] GetFileContent(string DocID, string DocType)
        {
            byte[] oReturn = null;

            try
            {
                if (DocType == "PDF")
                {
                    FileDataOptions oOptions;
                    oOptions.fileType = DocType;

                    oReturn = Proxy.GetFileData(DocID, oOptions);
                }
                else
                {
                    oReturn = Proxy.GetFileData(DocID);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return oReturn;
        }
    }

    [Serializable]
    public class FileMetaData
    {
        #region Declarations
        
        private string _DocID;
        public string filename;
        public int filesize;
        public string title;
        public string author;
        public string appsysid;
        public string appdoctype;

        private Object _pubdate;
        public Object pubdate
        {
            get 
            {
                try
                {
                    return ((DateTime)_pubdate).ToString("yyyy-MM-ddTHH:mm:ss"); 
                }
                catch (Exception)
                {
                    return null;
                }                
            }
            set { _pubdate = value; }
        }

        public MoreMetaData moreMetadata;

        #endregion

        #region Private Members

        private void Initialize(FileMetaData Data, string DocID)
        {
            _DocID = DocID;
            filename = Data.filename;
            filesize = Data.filesize;
            title = Data.title;
            author = Data.author;
            appsysid = Data.appsysid;
            appdoctype = Data.appdoctype;
            pubdate = Convert.ToDateTime(Data.pubdate);
            moreMetadata = Data.moreMetadata;
        }        

        #endregion

        #region Public Members

        public FileMetaData()
        {
        }

        public FileMetaData(string DocID)
        {
            try
            {
                XMLRPC oXMLRPC = new XMLRPC();
                Initialize(oXMLRPC.GetFileMetaData(DocID), DocID);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public bool     Update(string ID, string Name, string Description, string Author, string Addressee, string DateLabel, string sDocumentDate, string AddedBy, int SensitivityRationale)
        {
            bool oReturn = false;

            try
            {
                FileMetaDataUpdateOptions oOptions;
                oOptions.mergeMM = true;               
                this.title = Name;
                this.moreMetadata.docsummary = Description;
                this.moreMetadata.docauthor = Author;
                this.moreMetadata.addressee = Addressee;
                this.moreMetadata.datelabel = DateLabel;
                this.moreMetadata.docdate = sDocumentDate;
                this.moreMetadata.sensitiverat = SensitivityRationale.ToString();
                if (SensitivityRationale != 0)
                {
                    this.moreMetadata.sensitiveflag = "1";
                }
                else { this.moreMetadata.sensitiveflag = "0"; }
                XMLRPC oXMLRPC = new XMLRPC();
                oReturn = oXMLRPC.ReplaceFileMetaData(_DocID, this, oOptions);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return oReturn;
        }       

        //Added to update each field in UCM
        public bool UpdateField(string ID, string Name, string Description, string Author, string Addressee, string DateLabel, string sDocumentDate, string AddedBy, int SensitivityRationale, string Currentrow,string Currentcolumn,string Previousvalue,string Currentvalue)
        {
            bool oReturn = false;

            try
            {
                FileMetaDataUpdateOptions oOptions;
                oOptions.mergeMM = true;

                if (Convert.ToInt32(Currentcolumn) == 4)
                {
                    this.title = Name;
                }
                else if (Convert.ToInt32(Currentcolumn) == 5)
                {
                    this.moreMetadata.docsummary = Description;
                }
                else if (Convert.ToInt32(Currentcolumn) == 6)
                {
                    this.moreMetadata.docauthor = Author;
                }
                else if (Convert.ToInt32(Currentcolumn) == 7)
                {
                    this.moreMetadata.addressee = Addressee;
                }
                else if (Convert.ToInt32(Currentcolumn) == 8)
                {
                    this.moreMetadata.datelabel = DateLabel;
                }
                else if (Convert.ToInt32(Currentcolumn) == 9)
                {
                    this.moreMetadata.docdate = sDocumentDate;
                }
                else if (Convert.ToInt32(Currentcolumn) == 12)
                {
                    this.moreMetadata.sensitiverat = SensitivityRationale.ToString();
                }
                //this.title = Name;
                //this.moreMetadata.docsummary = Description;
                //this.moreMetadata.docauthor = Author;
                //this.moreMetadata.addressee = Addressee;
                //this.moreMetadata.datelabel = DateLabel;
                //this.moreMetadata.docdate = sDocumentDate;
                //this.moreMetadata.sensitiverat = SensitivityRationale.ToString();
                if (SensitivityRationale != 0)
                {
                    this.moreMetadata.sensitiveflag = "1";
                }
                else { this.moreMetadata.sensitiveflag = "0"; }
                XMLRPC oXMLRPC = new XMLRPC();
                oReturn = oXMLRPC.ReplaceFileMetaData(_DocID, this, oOptions);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return oReturn;
        }       

        #endregion
    }
}