﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml;
using System.Text;

namespace DMD
{
    public class Containers
    {
        #region Declarations

        private int iProjectID;
        private List<Container> lContainers;
        private List<Document> lDocuments;

        private List<Container> lContainerRepository;
        private Documents oDocumentRepository;

        public List<Container> Items
        {
            get
            {
                return lContainers;
            }
        }

        public List<Document> Documents
        {
            get
            {
                return lDocuments;
            }
        }

        #endregion

        #region Private members

        private void Load(string loadview)
        {
            try
            {
                string sXML = "";
                StringReader oReader = new StringReader(sXML);
                if (!string.IsNullOrEmpty(loadview))
                {
                    sXML = WADL.Get(Configuration.Projects + iProjectID + "/containers?viewId=" + loadview);
                }
                else
                {
                    sXML = WADL.Get(Configuration.Projects + iProjectID + "/containers?ignorecaraflag=1");
                }
                oReader = new StringReader(sXML);
                XmlDocument oDoc = new XmlDocument();
                oDoc.Load(oReader);
                XmlNode oParentNode = oDoc.ChildNodes[1];

                foreach (XmlNode oChildNode in oParentNode.ChildNodes)
                {
                    if (oChildNode.Name == "container")
                    {
                        Container oContainer = CreateContainer(oChildNode);
                        lContainers.Add(oContainer);
                        lContainerRepository.Add(oContainer);
                    }
                    else if (oChildNode.Name == "containerdoc")
                    {
                        if (oDocumentRepository != null)
                        {
                            Document oRepositoryDocument = oDocumentRepository.Items.Find(x => x.ID == oChildNode.Attributes["docid"].Value);
                            
                            lDocuments.Add(oRepositoryDocument);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private Container CreateContainer(XmlNode ContainerNode, Container parent = null)
        {
            Container oReturn;
            try
            {
                int iNodeID = Convert.ToInt32(ContainerNode.Attributes["contid"].Value);
                bool bHasFixedFlag = ContainerNode.Attributes["fixedflag"] != null;
                bool bFixedFlag;

                String strFixed = "false";
                if (bHasFixedFlag)
                {
                    strFixed = ContainerNode.Attributes["fixedflag"].Value;
                }

                bFixedFlag = Convert.ToBoolean(strFixed);

                oReturn = new Container(iNodeID, parent,
                                        Convert.ToInt32(ContainerNode.Attributes["order"].Value),
                                        ContainerNode.Attributes["label"].Value,
                                        bFixedFlag);

                foreach (XmlNode oChildNode in ContainerNode.ChildNodes)
                {
                    if (oChildNode.Name == "container")
                    {
                        Container oContainer = CreateContainer(oChildNode, oReturn);
                        oReturn.Containers.Add(oContainer);
                        lContainerRepository.Add(oContainer);
                    }
                    else if (oChildNode.Name == "containerdoc")
                    {
                        if (oDocumentRepository != null)
                        {
                            Document oRepositoryDocument = oDocumentRepository.Items.Find(x => x.ID == oChildNode.Attributes["docid"].Value);
                            oRepositoryDocument.ParentID = iNodeID;
                            oRepositoryDocument.AllowPublish = !bFixedFlag;
                            oRepositoryDocument.Order = Convert.ToInt32(oChildNode.Attributes["order"].Value);
                            oReturn.Documents.Add(oRepositoryDocument);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return oReturn;
        }

        private void Create(Containers original, Template template)
        {
            lDocuments.AddRange(original.Documents);
            ContainerTemplateMapping mapping = new ContainerTemplateMapping(original.lContainerRepository, template);

            //Let's map all of the mappable containers.
            foreach (TemplateItem item in template.items)
            {
                Container newContainer = CreateContainer(mapping, null, item);
                lContainers.Add(newContainer);
                lContainerRepository.Add(newContainer);
            }

            //We can't delete static containers, so if we have unmapped static containers
            //we'll add them back at the root level.  This will, hopefully, never happen
            foreach (Container container in mapping.unmatchedContainers)
            {
                if (container.Static)
                {
                    Container newContainer = Container.CreateFromContainer(null, container);
                    lContainers.Add(newContainer);
                    lContainerRepository.Add(newContainer);
                }
            }

            //create ids for any containers that don't have them.  We need this to save correctly
            //We need to wait until all of the containers have been added to the new structure
            //or the ID may not be correctly calculated.
            foreach (Container c in lContainerRepository)
            {
                if (c.ID == null)
                {
                    c.ID = getNextPalsContentId();
                }
            }

            //remap document parents.
            //This is a bit awkward because the documents will be shared between the 
            //source and new container trees but I couldn't think of a good way to 
            //do this as a seperate step and making a copy is unnecesary extra work.
            //The dependancies between Projects, Containers, and Documents are wierdly mixed
            //up and should be rethought
            foreach(Container c in original.lContainerRepository)
            {
                //if we mapped the container directly by ID, we can simply add the documents directly
                //static containers are either matched or specifically added after the fact and 
                //should always be treated as if they were matched.
                Container newContainer;
                if (mapping.matchedContainerIds.Contains(c.ID.Value) || c.Static)
                {
                    newContainer = GetContainerByID(c.ID);
                    newContainer.Documents.AddRange(c.Documents);
                }
                else
                {
                    //if we don't have a direct match but we have a transfer match (the second
                    //and subsequent category that matches a particular template item) then we 
                    //can look up the new container that me matched the item to.  Note that 
                    //we will always have a container based on an existing container (which deliberately
                    //preserves the same ID/record for reorganized tree) since if there wasn't an
                    //existing container the transfer would have been mapped directly
                    //we reset the document parents to ensure that it maps correctly
                    if (mapping.containerToItemTransfer.ContainsKey(c.ID.Value))
                    {
                        long itemID = mapping.containerToItemTransfer[c.ID.Value].itemid.Value;
                        newContainer = GetContainerByID(mapping.itemToContainer[itemID].ID);
                        if (newContainer != null)
                        {
                            foreach (Document d in c.Documents)
                            {
                                d.ParentID = newContainer.ID;
                                newContainer.Documents.Add(d);
                            }
                        }
                    }
                    else
                    {
                        //if we don't have a match for this container so make the document
                        //uncategorized (has no container)
                        foreach (Document d in c.Documents)
                        {
                            d.ParentID = null;
                            lDocuments.Add(d);
                        }
                    }
                }
            }
        }

        private Container CreateContainer(ContainerTemplateMapping mapping, Container parent, TemplateItem item)
        {
            //create the container
            Container match;
            Container newContainer;
            if (mapping.itemToContainer.TryGetValue(item.itemid.Value, out match))
            {
                //reuse the existing container (but make a new object since it might have a differnet parent)
                newContainer = Container.CreateFromContainer(parent, match);
            }
            else
            {
                //create a new container from the item
                newContainer = Container.CreateFromTemplateItem(parent, item);
            }

            //create and populate the children
            foreach (TemplateItem childitem in item.items)
            {
                Container child = CreateContainer(mapping, newContainer, childitem);
                newContainer.Containers.Add(child);
                lContainerRepository.Add(child);
            }

            return newContainer;
        }

        private byte[] GetCommitXml(Encoding encoding)
        {
            using (MemoryStream output = new MemoryStream())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = encoding;

                XmlWriter xml = XmlWriter.Create(output, settings);

                xml.WriteStartDocument();
                xml.WriteStartElement("containers", "http://www.fs.fed.us/nepa/schema");

                foreach (Container oContainer in lContainers)
                {
                    AddChildContainersXML(xml, oContainer);
                }

                xml.WriteEndElement();
                xml.WriteEndDocument();
                xml.Close();

                return output.ToArray();
            }
        }

        private void AddChildContainersXML(XmlWriter xml, Container oContainer)
        {
            xml.WriteStartElement("container");
            xml.WriteAttributeString("order", XmlConvert.ToString(oContainer.Order));

            if (oContainer.ID != null)
            {
                xml.WriteAttributeString("contid", XmlConvert.ToString(oContainer.ID.Value));
            }
            xml.WriteAttributeString("label", oContainer.Label);

            foreach (Container oChildContainer in oContainer.Containers)
            {
                AddChildContainersXML(xml, oChildContainer);
            }

            xml.WriteEndElement();
        }

        
        //the Datamart requires that the pals ids be unique by project
        //but doesn't always enforce it. This allows us to check before we send.
        private bool hasDupId()
        {
            HashSet<long> seen = new HashSet<long>();
            foreach (Container c in lContainerRepository)
            {
                //allow multiple null values
                if (c.ID != null)
                {
                    if (seen.Contains(c.ID.Value))
                    {
                        return false;
                    }
                    seen.Add(c.ID.Value);
                }
            }
            return true;
        }


        /*
            Note that this is dubious, but the Datamart will not autogenerate these IDs and
            despite the name/original source/fact that it is technically a nullable field
            the DM update function for container trees absolutely depends on this being set
            and unique to a project.  Given that it only has to be unique to a project and 
            we have the entire container tree present we can generate a new unique id with 
            an acceptable level of safety.
        */
        private long getNextPalsContentId()
        {
            long? maxID = lContainerRepository.Max(e => e.ID);

            //IDs can be negative, but these are special values.  We don't want to 
            //create negative IDs here.
            if (maxID == null || maxID < 1)
            {
                return 1;
            }

            return maxID.Value + 1;
        }

        #endregion

        #region Public members

        public static Containers CreateFromTemplate(Project project, Template template)
        {
            Containers containers = new Containers()
            {
                iProjectID = project.ID,
                oDocumentRepository = project.Documents
            };

            containers.Create(project.Containers, template);
            return containers;
        }

        public Containers(int ProjectID, Documents DocumentRepository = null) : this()
        {
            iProjectID = ProjectID;
            oDocumentRepository = DocumentRepository;
            Load("");
        }

        public Containers(int ProjectID, string loadview, Documents DocumentRepository = null) : this()
        {
            iProjectID = ProjectID;
            oDocumentRepository = DocumentRepository;
            Load(loadview);
        }

        private Containers()
        {
            lContainers = new List<Container>();
            lDocuments = new List<Document>();
            lContainerRepository = new List<Container>();
        }

        public Container GetContainerByID(long? ID)
        {
            return lContainerRepository.Find(x => x.ID == ID);
        }

        public bool Add(string ParentID, string Label, bool Static, out long NodeID)
        {
            bool bReturn = false;
            int iOrder = 0;

            try
            {
                NodeID = lContainerRepository.Max().ID.Value + 1;

                Container oParentContainer = GetContainerByID(Convert.ToInt32(ParentID));
                if (oParentContainer.Containers.Count > 0)
                {
                    iOrder = oParentContainer.Containers.Max().Order + 1;
                }
                Container oContainer = new Container(NodeID, oParentContainer, iOrder, Label, Static);
                oParentContainer.Containers.Add(oContainer);

                Commit();

                bReturn = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return bReturn;
        }

        public bool Remove(string NodeID)
        {
            bool bReturn = false;

            try
            {
                int value;
                Container oContainer;
                if (int.TryParse(NodeID, out value))
                {
                    oContainer = GetContainerByID(Convert.ToInt32(NodeID));
                    if (oContainer.Parent ==  null)
                    {
                        lContainers.Remove(oContainer);
                    }
                    else
                    {
                        oContainer.Parent.Containers.Remove(oContainer);
                    }

                    Commit();
                }
                else
                {
                    Document oDocument = oDocumentRepository.GetDocumentByID(NodeID);
                    oContainer = GetContainerByID(oDocument.ParentID);
                    if (oContainer != null)
                    {
                        oContainer.Documents.Remove(oDocument);
                    }
                    oDocumentRepository.Items.Remove(oDocument);
                    oDocument.Delete();
                }

                bReturn = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return bReturn;
        }

        public bool Update(string NodeID, string Label, bool Static)
        {
            bool bReturn = false;

            try
            {
                Container oContainer = GetContainerByID(Convert.ToInt32(NodeID));
                oContainer.Label = Label;
                oContainer.Static = Static;
                Commit();

                bReturn = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return bReturn;
        }

        public bool Move(string ID, string OldParentID, string NewParentContainerID, string after)
        {
            bool bReturn = false;

            try
            {
                int value;
                if (int.TryParse(ID, out value))
                {
                    //find the id of the container to move this after
                    long? afterid = null;
                    if (after != null && after != "")
                    {
                        long after_temp;
                        if (long.TryParse(after, out after_temp))
                        {
                            afterid = after_temp;
                        }
                    }

                    Container oContainer = GetContainerByID(Convert.ToInt32(ID));
                    //this is a direct child of the project -- we can move from the project root but not 
                    //to theproject root.
                    if (OldParentID == "" || OldParentID == "#")
                    {
                        lContainers.Remove(oContainer);
                    }
                    else
                    {
                        GetContainerByID(Convert.ToInt32(OldParentID)).Containers.Remove(oContainer);
                    }

                    GetContainerByID(Convert.ToInt32(NewParentContainerID)).AddChild(oContainer, afterid);

                    Commit();
                }
                else
                {
                    Document oDocument = oDocumentRepository.GetDocumentByID(ID);
                    GetContainerByID(Convert.ToInt32(NewParentContainerID)).Documents.Add(oDocument);

                    if (OldParentID == "" || OldParentID == "#")
                    {
                        lDocuments.Remove(oDocument);
                    }
                    else
                    {
                        GetContainerByID(Convert.ToInt32(OldParentID)).Documents.Remove(oDocument);
                    }
                    oDocument.ParentID = Convert.ToInt32(NewParentContainerID);
                    oDocumentRepository.Commit();
                }
                bReturn = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return bReturn;
        }

        public void Commit()
        {
            try
            {
                Encoding encoding = new UTF8Encoding(false);
                byte[] data = GetCommitXml(encoding);

                WADL.Upload(Method.PUT, Configuration.Projects + iProjectID + "/containers/template", data, encoding);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        #endregion

        #region nested Classes

        private class ContainerTemplateMapping
        {
            //non fixed items that map to existing containers.  We will try to reuse containers
            //that appear in both the template and the project
            public Dictionary<long, Container> itemToContainer { get; private set; }

            //we have multiple containers in the project that map to a single item in the template
            //we will preserve one container in the project and move any documents from other containers
            //to the one we preserve.
            public Dictionary<long, TemplateItem> containerToItemTransfer { get; private set; }

            //we could look this up in the itemToContainer list, but this allows to look things
            //up more easily and faster when doing mappings.  We could store and inverted
            //container to item map, but we don't really need it.
            public HashSet<long> matchedContainerIds { get; private set; }

            //we have containers in the project that do not map to items in the template
            public List<Container> unmatchedContainers { get; private set; }

            //we have containers in the project that can potentially map to multiple places in the
            //template.  For now treat as unmatched, but we'd eventually like to ask the user.
            public List<Container> ambiguousContainers { get; private set; }


            public ContainerTemplateMapping(List<Container> containers, Template template)
            {
                if (!template.hasIDs())
                {
                    throw new InvalidOperationException("Cannot apply an unsaved template.");
                }

                List<TemplateItem> items;

                items = new List<TemplateItem>();
                flattenItemList(items, template.items);

                generateMappings(containers, items);
            }

            private void flattenItemList(List<TemplateItem> dest, List<TemplateItem> newitems)
            {
                dest.AddRange(newitems);

                foreach(TemplateItem newitem in newitems)
                {
                    flattenItemList(dest, newitem.items);
                }
            }

            private void generateMappings(List<Container> containers, List<TemplateItem> items)
            {
                this.itemToContainer = new Dictionary<long, Container>();

                this.containerToItemTransfer = new Dictionary<long, TemplateItem>();

                this.unmatchedContainers = new List<Container>();
                this.ambiguousContainers = new List<Container>();
                this.matchedContainerIds = new HashSet<long>();

                //handle the fixed containers in our project
                Dictionary<long, TemplateItem> itemsByPalsId = items.ToDictionary(i => i.palsContainerId);
                foreach (Container container in containers.FindAll(c => c.Static))
                {
                    TemplateItem match;
                    if (itemsByPalsId.TryGetValue(container.ID.Value, out match))
                    {
                        if (!match.fixedFlag)
                        {
                            unmatchedContainers.Add(container);
                        }
                        else
                        {
                            itemToContainer.Add(match.itemid.Value, container);
                            matchedContainerIds.Add(container.ID.Value);
                        }
                    }
                    else
                    {
                        unmatchedContainers.Add(container);
                    }
                }

                //trim anything with a fixed flag, we don't want to match these at all.
                ILookup<string, TemplateItem> itemLabelLookup = items.FindAll(i => !i.fixedFlag).ToLookup(i => i.label);
                foreach (Container container in containers.FindAll(c => !c.Static))
                {
                    IEnumerable<TemplateItem> matcheditems = itemLabelLookup[container.Label];
                    long count = matcheditems.LongCount();
                    if (count == 1)
                    {
                        TemplateItem match = matcheditems.First();
                        if (itemToContainer.ContainsKey(match.itemid.Value))
                        {
                            containerToItemTransfer.Add(container.ID.Value, match);
                        }
                        else
                        {
                            itemToContainer.Add(match.itemid.Value, container);
                            matchedContainerIds.Add(container.ID.Value);
                        }
                    }
                    else if (count == 0)
                    {
                        unmatchedContainers.Add(container);
                    }
                    else
                    {
                        ambiguousContainers.Add(container);
                    }                
                }
            }
        }

        #endregion
    }
}
