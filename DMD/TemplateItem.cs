﻿using System.Collections.Generic;
using System.Xml;

namespace DMD
{
    public class TemplateItem
    {
        public long? itemid { get; private set; }
        public int order { get; private set; }
        public long palsContainerId { get; private set; }
        public bool fixedFlag { get; set; }
        public string label { get; set; }
        public List<TemplateItem> items { get; private set; }

        public static TemplateItem CreateFromContainer(Container container)
        {
            TemplateItem item = new TemplateItem()
            {
                itemid = null,
                order = container.Order,
                fixedFlag = container.Static,
                label = container.Label,
                palsContainerId = container.ID.Value
            };

            item.SetChildren(container);
            return item;
        }

        public static TemplateItem CreateFromXml(XmlElement itemElement)
        {
            TemplateItem item = new TemplateItem();

            item.itemid = XmlConvert.ToInt64(itemElement.GetAttribute("itemid"));
            item.order = XmlConvert.ToInt32(itemElement.GetAttribute("order"));
            item.fixedFlag = XmlConvert.ToBoolean(itemElement.GetAttribute("fixedflag"));
            item.label = itemElement.GetAttribute("label");
            item.palsContainerId = XmlConvert.ToInt64(itemElement.GetAttribute("palscontainerid"));

            foreach (XmlNode child in itemElement.ChildNodes)
            {
                if (child.Name == "container")
                {
                    item.items.Add(TemplateItem.CreateFromXml((XmlElement)child));
                }
            }
            return item;
        }

        public bool hasIDs()
        {
            if (itemid == null)
            {
                return false;
            }

            foreach(TemplateItem item in items)
            {
                if(!item.hasIDs())
                {
                    return false;
                }
            }

            return true;
        }
    

        private TemplateItem()
        {
            items = new List<TemplateItem>();
        }

        private void SetChildren(Container container)
        {
            foreach(Container child in container.Containers)
            {
                items.Add(TemplateItem.CreateFromContainer(child));
            }
        }
    }
}
