﻿using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace DMD
{
    /*
        For now we will only implement the header.  However in the future we may want to 
        pull all of the Unit data so we'll map it out as a Header/Full class like we do the templates
    */
    public class UnitHeader
    {
        public string unitid { get; protected set; }
        public string name { get; protected set; }

        public static List<UnitHeader> getHeaders()
        {
            //this will return full unit records from the DM.  We might want to implement
            //a header query there to reduce overall data size being requested.
            List<UnitHeader> headers = new List<UnitHeader>();
            string xml = WADL.Get("ref/units/unique");

            XmlDocument doc = new XmlDocument();
            doc.Load(new StringReader(xml));

            XmlNode parentNode = doc.DocumentElement;
            foreach (XmlNode child in parentNode.ChildNodes)
            {
                if (child.Name == "unit")
                {
                    headers.Add(createTemplateHeaderFromNode(child));
                }
            }

            return headers;
        }

        private static UnitHeader createTemplateHeaderFromNode(XmlNode node)
        {
            UnitHeader header = new UnitHeader();

            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name == "code")
                {
                    header.unitid = child.InnerText;
                }
                else if (child.Name == "name")
                {
                    header.name = child.InnerText;
                }
            }
            return header;
        }
    

        protected UnitHeader()
        {
        }
    }
}
