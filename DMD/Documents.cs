﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Data;

namespace DMD
{
    public class Documents
    {
       #region Declarations

       private int iProjectID;
       private string sProjectDocumentID;
       private List<Document> lDocuments;

       public List<Document> Items
       {
           get
           {
               return lDocuments;
           }
       }

       #endregion

       #region Private members

       private void Load(bool IncludeFileMetaData = false)
       {
	        string sAuthor = "";
            string sAddressee = "";
            string sDateLabel = "";
            int iContid; 
            int iSensitivityRationale;
            int iOut;
            int iOutCont;
            DateTime dDocumentDate;
            DateTime? dDate = null;
            DateTime? dDateAdded = null;
            string sAddedBy = "";
            
            DataSet oDocumentSet = new DataSet();

            var sXML = WADL.Get(Configuration.Projects + iProjectID + "/docs?ignorecaraflag=1");
            StringReader oReader = new StringReader(sXML);
            oDocumentSet.ReadXml(oReader);
            
            if (oDocumentSet.Tables.Count > 0)
            {
                foreach (DataRow oRow in oDocumentSet.Tables[0].Rows)
                {
                    if (oRow.Table.Columns.Contains("author")) {sAuthor =  oRow["author"].ToString();}
                    if (oRow.Table.Columns.Contains("addressee")) { sAddressee = oRow["addressee"].ToString(); }
                    if (oRow.Table.Columns.Contains("datelabel")) { sDateLabel = oRow["datelabel"].ToString(); }
                    if (oRow.Table.Columns.Contains("addedby")) { sAddedBy = oRow["addedby"].ToString(); }
                    if (oRow.Table.Columns.Contains("docdate"))
                    {
                        if (DateTime.TryParse(oRow["docdate"].ToString(), out dDocumentDate))
                        {
                            dDate = dDocumentDate;
                        }
                        else
                        {
                            dDate = null;
                        }
                    }
                    else
                    {
                        dDate = null;
                    }

                    if (oRow.Table.Columns.Contains("dateadded"))
                    {
                        if (DateTime.TryParse(oRow["dateadded"].ToString(), out dDocumentDate))
                        {
                            dDateAdded = dDocumentDate;
                        }
                        else
                        {
                            dDateAdded = null;
                        }
                    }
                    else
                    {
                        dDateAdded = null;
                    }

                    if (oRow.Table.Columns.Contains("sensitivityrationale"))
                    {
                        iSensitivityRationale = int.TryParse(oRow["sensitivityrationale"].ToString(), out iOut) ? iOut : 0;
                    }
                    else
                    {
                        iSensitivityRationale = 0;
                    }

                    if (oRow.Table.Columns.Contains("contid"))
                    {
                        iContid = int.TryParse(oRow["contid"].ToString(), out iOutCont) ? iOutCont : 0;
                    }
                    else
                    {
                        iContid = 0;
                    }
                        
                    lDocuments.Add(new Document(oRow["docid"].ToString(),
                                            oRow["docname"].ToString(),
                                            oRow["description"].ToString(),
                                            null,
                                            Convert.ToInt32(oRow["projectid"]),
                                            oRow["projecttype"].ToString(),
                                            oRow["pdffilesize"].ToString(),
                                            sAuthor, sAddressee, sDateLabel, dDate, sAddedBy, dDateAdded,
                                            Convert.ToBoolean(Convert.ToInt32(oRow["pubflag"].ToString())),
                                            iSensitivityRationale,
                                            iContid,
                                            IncludeFileMetaData,
                                            oRow["wwwlink"].ToString(),
                                            sProjectDocumentID));
                }
            }
       }

       #endregion

       #region Public members

       public Documents(int ProjectID, string ProjectDocumentID, bool IncludeFileMetaData = false)
       {
           iProjectID = ProjectID;
           sProjectDocumentID = ProjectDocumentID;
           lDocuments = new List<Document>();
           Load(IncludeFileMetaData);
       }

       public Document GetDocumentByID(string ID)
       {
           return lDocuments.Find(x => x.ID == ID);
       }

       public List<Document> GetDocumentsByParentID(long? ID)
       {
           return lDocuments.FindAll(x => x.ParentID == ID);
       }

       public bool ReOrderAll(long NodeID, DocumentSortOrder SortBy)
       {
            bool bReturn;

            try
            {
                // Get all documents from the current document's container
                List<Document> oDocuments = GetDocumentsByParentID(NodeID);

                switch (SortBy)
                {
                    case DocumentSortOrder.Alphabetical: 
                        oDocuments = oDocuments.OrderBy(x => x.Name).ToList();
                        break;
                    case DocumentSortOrder.DateAdded: 
                        oDocuments = oDocuments.OrderBy(x => x.DateAdded).ToList();
                        break;                  
                }                

                for (int i = 0; i < oDocuments.Count; i++)
                {
                    oDocuments[i].Order = i + 1;
                }

                Commit();
                bReturn = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

           return bReturn;
       }

       public bool ReOrder(string DocumentID, int NewPosition)
       {
           bool bReturn;

	       try
           {
               // Retrieve current document
               Document oDocument = GetDocumentByID(DocumentID);

               if (oDocument == null)
               {
                   throw new Exception("Invalid Document ID.");
               }

               // Get all documents from the current document's container
               List<Document> oDocuments = GetDocumentsByParentID(oDocument.ParentID);

               if (NewPosition < 0 || NewPosition >= oDocuments.Count)
               {
                   throw new Exception("Invalid new position, the value must be between 0 and " + (oDocuments.Count - 1).ToString() + ".");
               }

               // Sort documents and find the position of the current document
               oDocuments.Sort();                              
               var iCurrentPosition = oDocuments.IndexOf(oDocument);
               
               // Assign a new order number to the current document
               var iOrder = NewPosition == 0 ? 1 : oDocuments[NewPosition].Order + 1;
               oDocument.Order = iOrder;

               // Reorder all subsequent documents, if needed
               if (NewPosition < oDocuments.Count - 1 && oDocuments[NewPosition].Order <= iOrder)
               {
                   for (int i = NewPosition + (iCurrentPosition < NewPosition ? 1 : 0); i < oDocuments.Count; i++)
                   {
                       if (oDocuments[i].ID != oDocument.ID)
                       {
                           oDocuments[i].Order = ++iOrder;
                       }
                   }
               }

               Commit();
               bReturn = true;
           }
           catch (Exception e)
           {
               throw new Exception(e.Message);
           }

           return bReturn;
       }

       public void Commit()
       {
           try
           {
               var sInputXML = lDocuments.Aggregate(
                   "<containerdocs xmlns=\"http://www.fs.fed.us/nepa/schema\">", 
                   (current, oDocument) => current +
                        "<containerdoc order=\"" + oDocument.Order + "\" pubflag=\"" + (oDocument.Public ? "1" : "0") + "\" docid=\"" + oDocument.ID + 
                        (oDocument.ParentID != null ? "\" parentcontid=\"" + oDocument.ParentID  : "")  + "\" />");

	           sInputXML = sInputXML + "</containerdocs>";

               WADL.Upload(Method.POST, Configuration.Projects + iProjectID + "/containers/updatecontainerdocs?forceparent=true", sInputXML);
           }
           catch (Exception e)
           {
               throw new Exception(e.Message);
           }
       }

       #endregion   
    }
}
