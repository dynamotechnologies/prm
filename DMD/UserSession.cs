﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;

namespace DMD
{
    public class UserSession
    {
        #region Declarations

        public enum Permission
        {
            Read,
            Edit
        }

        public string UserName { get; private set; }
        public string ProjectID { get; private set; }
        public string Token { get; private set; }
        public DateTime? LastActive { get; private set; }
        public DateTime? Created { get; private set; }

        private string AccessLevel;

        #endregion


        #region Public members

        public UserSession(string Token)
        {
            try
            {
                // Retrieve existing user session
                this.Token = Token;
                Load();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public UserSession(string UserName, string ProjectID, string ProjectAccessLevel)
        {
            // Create a new user session
            this.UserName = UserName;
            this.ProjectID = ProjectID;
            //this.Token = Token;

            Encoding encoding = new UTF8Encoding(false);
            byte[] data;
            using (MemoryStream output = new MemoryStream())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = encoding;
                XmlWriter xml = XmlWriter.Create(output, settings);

                xml.WriteStartDocument();
                xml.WriteStartElement("usersession", "http://www.fs.fed.us/nepa/schema");

                xml.WriteElementString("userid", UserName);
                xml.WriteElementString("projectid", ProjectID);
                xml.WriteElementString("projectaccesslevel", ProjectAccessLevel);

                xml.WriteEndElement();
                xml.WriteEndDocument();
                xml.Close();

                data = output.ToArray();
            }

            string response = WADL.Upload(Method.POST, Configuration.UserSessions, data, encoding);
            setFromXml(response);
        }

        public bool hasPermission(UserSession.Permission perm)
        {
            if (perm == UserSession.Permission.Edit)
            {
                return (AccessLevel == "write");
            }
            else if (perm == UserSession.Permission.Read)
            {
                return true;
            }

            return false;
        }

        public void checkPermission(UserSession.Permission perm)
        {
            if (!hasPermission(perm))
            {
                throw new Exception("User does not have permission " + perm.ToString() + " on project id # " + ProjectID);
            }
        }

        #endregion

        #region Private members

        private void Load()
        {
            string sXML;

            try
            {
                sXML = WADL.Get(Configuration.UserSessions + Token);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.Contains("404") ? "User session does not exist." : e.Message);
            }

            setFromXml(sXML);
        }

        private void setFromXml(string sXML)
        {
            DateTime dDate;
            DataSet oDocumentSet = new DataSet();
            StringReader oReader;
            XmlDocument oDoc;

            oReader = new StringReader(sXML);
            oDoc = new XmlDocument();
            oDoc.Load(oReader);

            XmlNode oParentNode = oDoc.ChildNodes[1];

            foreach (XmlNode oChildNode in oParentNode.ChildNodes)
            {
                if (oChildNode.Name == "userid")
                {
                    UserName = oChildNode.InnerText;
                }
                else if (oChildNode.Name == "projectid")
                {
                    ProjectID = oChildNode.InnerText;
                }
                else if (oChildNode.Name == "token")
                {
                    Token = oChildNode.InnerText;
                }
                else if (oChildNode.Name == "projectaccesslevel")
                {
                    AccessLevel = oChildNode.InnerText;
                }
                else if (oChildNode.Name == "lastactivity")
                {
                    if (DateTime.TryParse(oChildNode.InnerText, out dDate))
                    {
                        LastActive = dDate;
                    }
                }
                else if (oChildNode.Name == "created")
                {
                    if (DateTime.TryParse(oChildNode.InnerText, out dDate))
                    {
                        Created = dDate;
                    }
                }
            }
        }

        #endregion
    }
}
