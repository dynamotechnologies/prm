﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Xml;

namespace DMD
{
    public class User
    {
        #region Declarations

        public string UserID { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }

        private List<UnitRole> UnitRoles;

        #endregion

        #region Public members

        public User(string UserID)
        {
            this.UserID = UserID;
            Load();
        }

        public bool HasRole(string sUnitCode)
        {
            foreach (UnitRole aRole in UnitRoles)
            {
                if (aRole.UnitCode.Equals(sUnitCode))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion


        #region Private members

        private void Load()
        {
            try
            {
                string sXML = WADL.Get(Configuration.Users + UserID);

                XmlDocument oDoc = new XmlDocument();
                oDoc.Load(new StringReader(sXML));
                
                XmlNode oParentNode = oDoc.ChildNodes[1];

                UnitRoles = new List<UnitRole>();
                foreach (XmlNode child in oParentNode.ChildNodes)
                {
                    if (child.Name == "unitroles")
                    {
                        foreach (XmlNode roleNode in child.ChildNodes)
                        {
                            UnitRoles.Add(UnitRole.createFromNode(roleNode));
                        }
                    }
                    else if(child.Name == "firstname")
                    {
                        FirstName = child.InnerText;
                    }
                    else if (child.Name == "lastname")
                    {
                        LastName = child.InnerText;
                    }
                    else if (child.Name == "email")
                    {
                        Email = child.InnerText;
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        #endregion

    }

    public class UnitRole
    {
        #region Declarations

        public string UnitCode { get; private set; }
        public int RoleID { get; private set; }

        #endregion

        #region Public members

        public static UnitRole createFromNode(XmlNode unitnode)
        {
            UnitRole role = new UnitRole();

            foreach (XmlNode child in unitnode.ChildNodes)
            {
                if (child.Name == "unit")
                {
                    role.UnitCode = child.InnerText;
                }

                else if (child.Name == "role")
                {
                    role.RoleID = XmlConvert.ToInt32(child.InnerText);
                }
            }
            return role;
        }

        private UnitRole()
        {
        }
        
        #endregion
    }
}
