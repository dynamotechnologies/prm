﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FolderView.aspx.cs" Inherits="PRM.FolderView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style2
        {
            width: 6px;
        }
        .style3
        {
            width: 366px;
        }
        .style4
        {
            width: 9%;
        }
    </style>
</head>
<body style="width: 2279px">
    <form id="form1" runat="server">
    <div style="width: 2175px">
    
        <table style="width:100%;">
        <tr><td class="style4">
            <asp:Button ID="btnFolderView" runat="server" onclick="btnFolderView_Click" 
                Text="Folder View" Width="95px" />
            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
            <asp:Button ID="btnIndexView" runat="server" onclick="btnIndexView_Click" 
                Text="Index View" Width="95px" />
            </td><td></td><td>
            <asp:Label ID="lblFolderName" runat="server" Text="Label"></asp:Label>
            </td></tr>
            <tr>
                <td valign="top" class="style4">
    
        <asp:GridView ID="gvProjectFolders" runat="server" CellPadding="4" ForeColor="#333333" 
            GridLines="None" Width="200px" AutoGenerateColumns="False" 
            Font-Names="Verdana" Font-Size="X-Small" onrowcommand="gvProjectFolders_RowCommand" 
                        BorderColor="#CCCCCC" BorderStyle="Solid">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:ButtonField DataTextField="Label" HeaderText="Project Folders" 
                    Text="Button" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    
                </td>
                <td class="style2" width="5%">
                    &nbsp;</td>
                <td align="left" valign="top" class="style3" width="75%">
    <asp:TreeView ID="tvContainers" Header="{Binding Path=Attribute[order].Value}" 
        runat="server" Font-Names="Verdana" Font-Size="X-Small" Width="100%" BorderColor="#CCCCCC" 
                        BorderStyle="Solid">
    </asp:TreeView>                   
    
        <asp:GridView ID="gvIndexView" runat="server" CellPadding="4" ForeColor="#333333" 
            GridLines="None" Width="1960px" AutoGenerateColumns="False" 
            Font-Names="Verdana" Font-Size="X-Small" onrowcommand="gvProjectFolders_RowCommand" 
                        BorderColor="#CCCCCC" BorderStyle="Solid">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="ProjectType" HeaderText="Project Type" />
                <asp:CheckBoxField DataField="Public" HeaderText="Public" />
                <asp:HyperLinkField DataNavigateUrlFields="Link" HeaderText="Link" 
                    Text="Link" />
                <asp:BoundField DataField="Name" HeaderText="Name" />
                <asp:BoundField DataField="Description" HeaderText="Description" />
                <asp:BoundField DataField="PdfFileSize" HeaderText="PDF File Size" />
                <asp:CheckBoxField DataField="Sensitive" HeaderText="Sensitive" />
                <asp:BoundField DataField="Order" HeaderText="Order" />
                <asp:BoundField DataField="ContainerID" HeaderText="Container ID" />
                <asp:BoundField DataField="ContainerOrder" HeaderText="Container Order" />
                <asp:BoundField DataField="ContainerLabel" HeaderText="Container Label" />
                <asp:BoundField DataField="ID" HeaderText="ID" />
                <asp:BoundField DataField="FileName" HeaderText="File Name" />
                <asp:BoundField DataField="FileSize" HeaderText="File Size" />
                <asp:BoundField DataField="Title" HeaderText="Title" />
                <asp:BoundField DataField="Author" HeaderText="Author" />
                <asp:BoundField DataField="PubDate" HeaderText="PubDate" />
                <asp:BoundField DataField="AppsysID" HeaderText="AppsysID" />
                <asp:BoundField DataField="DocType" HeaderText="Doc Type" />
                <asp:BoundField DataField="DocDate" HeaderText="DocDate" />
                <asp:BoundField DataField="ProjectID" HeaderText="Project ID" />
                <asp:BoundField DataField="DateLabel" HeaderText="Date Label" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    
                </td>
            </tr>
        </table>    
    </div>
    </form>
</body>
</html>
