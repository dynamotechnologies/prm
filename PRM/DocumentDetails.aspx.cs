﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DMD;

namespace PRM
{
    public partial class DocumentDetails : System.Web.UI.Page
    {
        static string sPreviousPage;
        static Document oDocument;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var sProjectID = Request.QueryString["ProjectID"];
                var sDocID = Request.QueryString["DocID"];

                sPreviousPage = Request.UrlReferrer.ToString();

                try
                {
                    oDocument = new Document(sDocID, sProjectID);

                    //Display document general information
                    txtDocID.Text = oDocument.ID;
                    txtProjectID.Text = oDocument.ProjectID.ToString();
                    txtProjectType.Text = oDocument.ProjectType;
                    chkPubFlag.Checked = oDocument.Public;
                    //txtLink.Text = oDocument.DownloadOriginal;
                    txtDocName.Text = oDocument.Name;
                    txtDescription.Text = oDocument.Description;
                    txtPdfFileSize.Text = oDocument.PDFfileSize.ToString();

                    //Display file metadata
                    lblFileNameValue.Text = oDocument.FileMetaData.filename;
                    lblFileSizeValue.Text = oDocument.FileMetaData.filesize.ToString();
                    lblTitleValue.Text = oDocument.FileMetaData.title;
                    lblAuthorValue.Text = oDocument.FileMetaData.author;
                    lblPubDateValue.Text = oDocument.FileMetaData.pubdate.ToString();
                    lblAppSysIDValue.Text = oDocument.FileMetaData.appsysid;
                    lblAppDocTypeValue.Text = oDocument.FileMetaData.appdoctype;
                    //txtDocDate.Text = oDocument.FileMetaData.moreMetadata.docdate;
                    //lblProjectIDValue.Text = oDocument.FileMetaData.moreMetadata.projectid;
                    //txtDateLabel.Text = oDocument.FileMetaData.moreMetadata.datelabel; 
                }
                catch (Exception oException)
                {
                    Response.Clear();
                    Response.Write("<h1>" + oException.Message + "</h1>");
                    Server.ClearError();
                }         
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            oDocument.Public = chkPubFlag.Checked;
            oDocument.Name = txtDocName.Text;
            oDocument.Description = txtDescription.Text;

            //oDocument.FileMetaData.moreMetadata.datelabel = txtDateLabel.Text;
            //oDocument.FileMetaData.moreMetadata.docdate = txtDocDate.Text;

            oDocument.Update();

            Response.Redirect(sPreviousPage);
        }
    }
}