﻿using System;
using System.IO;
using System.Net;
using System.Xml;

namespace PRM
{
    public partial class Default : System.Web.UI.Page
    {
        protected void btnGetDocuments_Click(object sender, EventArgs e)
        {
			Response.Redirect("DocDetails.html?pid=" + txtProjectID.Text + "&uid=" + txtUsername.Text);
        }
        protected void btnGetContainers_Click(object sender, EventArgs e)
        {
            Response.Redirect("Container.aspx?ProjectID=" + txtProjectID.Text);
        }

        protected void btnGetContainers0_Click(object sender, EventArgs e)
        {
            Response.Redirect("FolderView.aspx?ProjectID=" + txtProjectID.Text);
        }

        protected void btnGetProject_Click(object sender, EventArgs e)
        {
            string token = "";

            string proj_id = txtProjectID.Text;
            string user_id = txtUsername.Text;
            string password = txtPassword.Text;

            string accessLevel = "write";
            if (Request.Form["accessLevel"] != null)
            {
                if (Request.Form["accessLevel"].ToString() == "rbRead")
                {
                    accessLevel = "read";
                }
            }

            Config conf = new Config();

            var tokenURL = conf.SERVICE_PATH + "Main.svc/authentication/gettoken?project_id=";
            tokenURL += proj_id + "&pals_pw=" + password + "&userid=" + user_id + "&accessLevel=" + accessLevel;

            var oRequest = (HttpWebRequest)WebRequest.Create(tokenURL);

            //Get the response using the request
            HttpWebResponse oResponse = oRequest.GetResponse() as HttpWebResponse;
           
            //Read the stream from the response object
            Stream oStream = oResponse.GetResponseStream();
            StreamReader oStreamReader = new StreamReader(oStream);

            // Read the result from the stream reader
            string sXML = oStreamReader.ReadToEnd();
            StringReader oReader = new StringReader(sXML);           

            XmlDocument oDoc = new XmlDocument();
            oDoc.Load(oReader);

            XmlNode oParentNode = oDoc.ChildNodes[0];
            token = oParentNode.InnerText;

            Response.Redirect("FolderManagement.html?token=" + token);
        }
    }
}
