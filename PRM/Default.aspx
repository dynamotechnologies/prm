﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PRM.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">
        .textbox
        {
            width:150px;
        }

        .label 
        {
            display: inline-block;
            width: 100px;
            margin-right: 5px;
        }

        .input
        {
            margin-top: 10px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Panel class="input" runat="server">
            <asp:Label class="label" ID="lblProjectID" runat="server" Text="Project ID: "></asp:Label>
            <asp:TextBox class="textbox" ID="txtProjectID" runat="server"></asp:TextBox>
        </asp:Panel>

        <asp:Panel class="input" runat="server">
            <asp:Label class="label" ID="lblUsername" runat="server" Text="Username: "></asp:Label>
            <asp:TextBox class="textbox" ID="txtUsername" runat="server"></asp:TextBox>
        </asp:Panel>

        <asp:Panel class="input" runat="server">
            <asp:Label class="label" ID="lblAccess" runat="server" Text="Access Level: "></asp:Label>
            <asp:RadioButton ID="rbRead" GroupName="accessLevel" Text="Read" runat="server" />
            <asp:RadioButton ID="rbWrite" GroupName="accessLevel" Text="Write" Checked="true" runat="server" />
        </asp:Panel>

        <asp:Panel class="input" runat="server">
            <asp:Label class="label" ID="lblPassword" runat="server" Text="Password: "></asp:Label>
            <asp:TextBox class="textbox" ID="txtPassword" runat="server"></asp:TextBox>
        </asp:Panel>

<!--
        <asp:Button ID="btnGetDocuments" runat="server" Height="28px" 
            onclick="btnGetDocuments_Click" Text="Get documents" />
    
        <asp:Button ID="btnGetContainers" runat="server" Height="28px" 
            onclick="btnGetContainers_Click" Text="Get containers" />
    
        <asp:Button ID="btnGetFolderView" runat="server" Height="28px" 
            onclick="btnGetContainers0_Click" Text="Folder view" />
-->
        <br /><br />
        <asp:Button ID="btnGetProject" runat="server" Height="28px" onclick="btnGetProject_Click" Text="Test PRM" />    
    </div>
    </form>
</body>
</html>
