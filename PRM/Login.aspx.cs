﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;

namespace PRM
{
    public partial class Login : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            string token = Request["token"]; 

            Config conf = new Config();
            var tokenURL = conf.SERVICE_PATH + "Main.svc/Authentication/sessioninfo?token=" + token;
            var oRequest = (HttpWebRequest)WebRequest.Create(tokenURL);

            //Get the response using the request
            HttpWebResponse oResponse = oRequest.GetResponse() as HttpWebResponse;

            //Read the stream from the response object
            Stream oStream = oResponse.GetResponseStream();
            StreamReader oStreamReader = new StreamReader(oStream);

            // Read the result from the stream reader
            string json = oStreamReader.ReadToEnd();

            //double json encoded for your convenience
            JObject result = JObject.Parse(json);
            JObject session = JObject.Parse((string) result["GetSessionInfoResult"]);
        
            Boolean canedit = (Boolean)session["canedit"];

            if (canedit)
            {
                Response.Redirect("FolderManagement.html?token=" + token);
            }
            else
            {
                Response.Redirect("DocDetails.html?token=" + token);
            }
        }
	}
}
