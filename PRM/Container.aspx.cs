﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMD;

namespace PRM
{
    public partial class Container : System.Web.UI.Page
    {
        static Project oProject;

        protected void Page_Load(object sender, EventArgs e)
        {
            var sProjectID = Request.QueryString["ProjectID"];

            try
            {
                oProject = new Project(Convert.ToInt32(sProjectID));

                TreeNode oNode = new TreeNode();
                oNode.Text = "Containers";
                tvContainers.Nodes.Add(oNode);

                AddChildNodes(ref oNode, oProject.Containers.Items, oProject.Containers.Documents);
           }
            catch (Exception oException)
            {
                Response.Clear();
                Response.Write("<h1>" + oException.Message + "</h1>");
                Server.ClearError();
            }           
        }

        private void AddChildNodes(ref TreeNode ParentNode, List<DMD.Container> ChildContainers, List<DMD.Document> ChildDocuments)
        {
            foreach (DMD.Container oContainer in ChildContainers)
            {
                TreeNode oNode = new TreeNode();
                if (oContainer.Containers.Count > 0 || oContainer.Documents.Count > 0)
                {
                    AddChildNodes(ref oNode, oContainer.Containers, oContainer.Documents);
                }                
                oNode.Text = oContainer.Label;
                oNode.SelectAction = TreeNodeSelectAction.None;
                ParentNode.ChildNodes.Add(oNode);
            }

            foreach (DMD.Document oDocument in ChildDocuments)
            {
               TreeNode oNode = new TreeNode();
               oNode.Text = oDocument.Name + " (" + oDocument.ID + ")";
               oNode.NavigateUrl = oDocument.DownloadOriginal;
               oNode.SelectAction = TreeNodeSelectAction.Select;
               ParentNode.ChildNodes.Add(oNode);
            }
        }
    }
}