﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMD;
using System.Data;

namespace PRM
{
    public partial class FolderView : System.Web.UI.Page
    {
        static Project oProject;

        protected void Page_Load(object sender, EventArgs e)
        {
            var sProjectID = Request.QueryString["ProjectID"];

            try
            {
                if (oProject == null || sProjectID != oProject.ID.ToString())
                {
                    oProject = new Project(Convert.ToInt32(sProjectID), ProjectInfo.Full, true);

                    //Populate the datatable
                    gvProjectFolders.DataSource = oProject.Containers.Items;
                    gvProjectFolders.DataBind();

                    AddFolderItems(0);

                    tvContainers.Visible = true;
                    gvIndexView.Visible = false;
                    lblFolderName.Visible = false;
                }
            }
            catch (Exception oException)
            {
                Response.Clear();
                Response.Write("<h1>" + oException.Message + "</h1>");
                Server.ClearError();
            }    
        }

        private void AddChildNodes(ref TreeNode ParentNode, List<DMD.Container> ChildContainers, List<DMD.Document> ChildDocuments)
        {
            TreeNode oNode;
            string sToolTip;

            foreach (DMD.Container oContainer in ChildContainers)
            {
                oNode = new TreeNode();
                if (oContainer.Containers.Count > 0 || oContainer.Documents.Count > 0)
                {
                    AddChildNodes(ref oNode, oContainer.Containers, oContainer.Documents);
                }
                oNode.Text = oContainer.Label;
                oNode.SelectAction = TreeNodeSelectAction.None;
                ParentNode.ChildNodes.Add(oNode);
            }

            foreach (DMD.Document oDocument in ChildDocuments)
            {
                oNode = new TreeNode();
                oNode.Text = oDocument.Name + " (" + oDocument.ID + ")";
                oNode.NavigateUrl = oDocument.DownloadOriginal;
                oNode.SelectAction = TreeNodeSelectAction.Select;

                sToolTip = "Order: " + oDocument.Order + "\r";
                sToolTip = sToolTip + "Doc ID: " + oDocument.ID + "\r";
                sToolTip = sToolTip + "Project ID: " + oDocument.ProjectID + "\r";
                sToolTip = sToolTip + "Project Type ID: " + oDocument.ProjectType + "\r";
                sToolTip = sToolTip + "Pub Flag: " + oDocument.Public + "\r";
                sToolTip = sToolTip + "Link: " + oDocument.DownloadOriginal + "\r";
                sToolTip = sToolTip + "Doc Name: " + oDocument.Name + "\r";
                sToolTip = sToolTip + "Description: " + oDocument.Description + "\r";
                sToolTip = sToolTip + "PDF File Size: " + oDocument.PDFfileSize + "\r";
                sToolTip = sToolTip + "MetaData/FileName: " + oDocument.FileMetaData.filename + "\r";
                sToolTip = sToolTip + "MetaData/FileSize: " + oDocument.FileMetaData.filesize + "\r";
                sToolTip = sToolTip + "MetaData/Title: " + oDocument.FileMetaData.title + "\r";
                sToolTip = sToolTip + "MetaData/Author: " + oDocument.FileMetaData.author + "\r";
                sToolTip = sToolTip + "MetaData/PubDate: " + oDocument.FileMetaData.pubdate + "\r";
                sToolTip = sToolTip + "MetaData/AppsysID: " + oDocument.FileMetaData.appsysid + "\r";
                sToolTip = sToolTip + "MetaData/AppDocType: " + oDocument.FileMetaData.appdoctype + "\r";
                //sToolTip = sToolTip + "MetaData/MoreMetaData/DocDate: " + oDocument.FileMetaData.moreMetadata.docdate + "\r";
                //sToolTip = sToolTip + "MetaData/MoreMetaData/ProjectID: " + oDocument.FileMetaData.moreMetadata.projectid + "\r";
                //sToolTip = sToolTip + "MetaData/MoreMetaData/DocLable: " + oDocument.FileMetaData.moreMetadata.datelabel + "\r";

                oNode.ToolTip = sToolTip;

                ParentNode.ChildNodes.Add(oNode);
            }
        }

        private void AddFolderItems(int FolderIndex)
        {
            tvContainers.Nodes.Clear();

            TreeNode oNode = new TreeNode();
            oNode.Text = oProject.Containers.Items[FolderIndex].Label + " Folder";
            oNode.SelectAction = TreeNodeSelectAction.None;
            tvContainers.Nodes.Add(oNode);

            AddChildNodes(ref oNode, oProject.Containers.Items[FolderIndex].Containers, oProject.Containers.Items[FolderIndex].Documents);

            lblFolderName.Text = oProject.Containers.Items[FolderIndex].Label + " Folder";

            DataTable oIndexTable = new DataTable();
            oIndexTable.Columns.Add("ProjectType", typeof(string));
            oIndexTable.Columns.Add("Public", typeof(bool));
            oIndexTable.Columns.Add("Link", typeof(string));
            oIndexTable.Columns.Add("Name", typeof(string));
            oIndexTable.Columns.Add("Description", typeof(string));
            oIndexTable.Columns.Add("Sensitive", typeof(string));
            oIndexTable.Columns.Add("PdfFileSize", typeof(string));
            oIndexTable.Columns.Add("Order", typeof(string));
            oIndexTable.Columns.Add("ContainerID", typeof(string));
            oIndexTable.Columns.Add("ContainerOrder", typeof(string));
            oIndexTable.Columns.Add("ContainerLabel", typeof(string));
            oIndexTable.Columns.Add("ID", typeof(string));
            oIndexTable.Columns.Add("FileName", typeof(string));
            oIndexTable.Columns.Add("FileSize", typeof(string));
            oIndexTable.Columns.Add("Title", typeof(string));
            oIndexTable.Columns.Add("Author", typeof(string));
            oIndexTable.Columns.Add("PubDate", typeof(string));
            oIndexTable.Columns.Add("AppsysID", typeof(string));
            oIndexTable.Columns.Add("DocType", typeof(string));
            oIndexTable.Columns.Add("DocDate", typeof(string));
            oIndexTable.Columns.Add("ProjectID", typeof(string));
            oIndexTable.Columns.Add("DateLabel", typeof(string));

            foreach(DMD.Document oDocument in oProject.Containers.Items[FolderIndex].Documents)
            {
                oIndexTable.Rows.Add(oDocument.ProjectType,
                                     oDocument.Public,
                                     oDocument.DownloadOriginal,
                                     oDocument.Name,
                                     oDocument.Description,
                                     oDocument.SensitivityRationale,
                                     oDocument.PDFfileSize,
                                     oDocument.Order,
                                     oProject.Containers.Items[FolderIndex].ID,
                                     oProject.Containers.Items[FolderIndex].Order,
                                     oProject.Containers.Items[FolderIndex].Label,
                                     oDocument.ID,
                                     oDocument.FileMetaData.filename,
                                     oDocument.FileMetaData.filesize,
                                     oDocument.FileMetaData.title,
                                     oDocument.FileMetaData.author,
                                     oDocument.FileMetaData.pubdate,
                                     oDocument.FileMetaData.appsysid,
                                     oDocument.FileMetaData.appdoctype,
                                     "","","");
            }

            gvIndexView.DataSource = oIndexTable;
            gvIndexView.DataBind();
        }

        protected void gvProjectFolders_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);

            AddFolderItems(index);           
        }

        protected void btnIndexView_Click(object sender, EventArgs e)
        {
            tvContainers.Visible = false;
            gvIndexView.Visible = true;
            lblFolderName.Visible = true;
        }

        protected void btnFolderView_Click(object sender, EventArgs e)
        {
            tvContainers.Visible = true;
            gvIndexView.Visible = false;
            lblFolderName.Visible = false;
        }
    }
}