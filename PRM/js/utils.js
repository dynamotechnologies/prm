﻿function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1];
        }
    }
}

function loadScript(src, onLoad) {
    var scriptTag = document.createElement("script");

    scriptTag.src = src;

    if (typeof onLoad === "function") {
        scriptTag.onload = onLoad;
        scripTag.onreadystatechange = function () {
            if (scriptTag.readyState === 4) {
                onLoad();
            }
        }
    }

    document.body.appendChild(scriptTag);
}

function shorten(text, maxLength) {
	var ret = text;
	if (ret.length > maxLength) {
		ret = ret.substr(0, maxLength - 3) + "...";
	}
	return ret;
}

//namespace
var PRM = {};
//sending viewid to downloader UI
var downloadviewid;
var curURL = window.location.toString();

    if (curURL.indexOf('viewid=') > -1) {
        downloadviewid = curURL.split('viewid=')[1];
    }
PRM.appClass = function () {}
PRM.appClass.prototype =
{
    initialize: function () {
        this.token = GetQueryStringParams('token');

        this.projectID = null;
        this.userID = null;
        this.canedit = null;

        var appObj = this;

        //load the token info and finish initializing the page
        $.ajax({
            url: SERVICE_PATH + "Main.svc/project/getinfo?token=" + token,
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" },

            success: function (data) {
                var obj = jQuery.parseJSON(data.GetProjectInfoResult);
                appObj.projectID = obj.project.ID;
                appObj.userID = obj.userinfo.userid;
                appObj.canedit = obj.userinfo.canedit;

                //set some things based on the returned data.
                appObj.addUserMessage("Project " + obj.project.ID + " - " + obj.project.Name + " - Last Updated: " + obj.project.LastUpdate);

                //let's be safety consious here and make sure nothing weird happens with our paramters.
                var author = 'author=' + encodeURIComponent(appObj.userID);
                var project = 'projectid=' + encodeURIComponent(appObj.projectID);
                var webProject = 'project=' + encodeURIComponent(appObj.projectID);
                var token = 'token=' + encodeURIComponent(appObj.token);
                var viewid = 'viewid=' + encodeURIComponent(downloadviewid);

                $('.js-menu__link_manage').prop('href', 'FolderManagement.html?' + token);
                $('.js-menu__link_details').prop('href', 'DocDetails.html?' + token);

                //$('.js-menu__link_upload').prop('href', UPLOADER_PATH + '?appsysid=01&' + author + '&' + project + '&source=pfm');
                //open uploader in new window
                $('.js-menu__link_upload').on("click", function () { openUploadWin(UPLOADER_PATH + '?appsysid=01&' + author + '&' + project + '&source=pfm'); }); 

                if (downloadviewid != null && downloadviewid != '') { //if load view is not null disable the whole tree
                    $('.js-menu__link_download').prop('href', DOWNLOADER_PATH + '?' + project + '&' + author + '&' + viewid);
                }
                else {
                    $('.js-menu__link_download').prop('href', DOWNLOADER_PATH + '?' + project + '&' + author);
                }
                $('.js-menu__link_efile').prop('href', EFILE_PATH + '?' + project + '&' + author);
                $('.js-menu__link_pals').prop('href', PALS_PATH);
                $('.js-menu__link_webview').prop('href', WEBVIEW_PATH + '?' + webProject);

                //unhide anything that requires edit privs to see
                if (appObj.canedit) {
                    $('.js-edit-only').removeClass('hide');
                }
            },
            error: function (data) {
                console.log(data);
                alert("Failed to load project data");
            }
        });
    },

    addUserMessage: function (message) {
        $('#userMessage').append(message);
    }
}