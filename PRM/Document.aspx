﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Document.aspx.cs" Inherits="PRM.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:GridView ID="gvDocs" runat="server" CellPadding="4" ForeColor="#333333" 
            GridLines="None" Width="100%" AutoGenerateColumns="False" 
            Font-Names="Verdana" Font-Size="Small">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle Width="10%" />
                </asp:BoundField>
                <asp:CheckBoxField DataField="Public" HeaderText="Public" />
                <asp:BoundField DataField="Name" HeaderText="Name">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle Width="40%" />
                </asp:BoundField>
                <asp:BoundField DataField="Description" HeaderText="Description">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle Width="35%" />
                </asp:BoundField>
                <asp:HyperLinkField DataNavigateUrlFields="Link" HeaderText="Document" 
                    Text="Display">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle Width="5%" />
                </asp:HyperLinkField>
                <asp:HyperLinkField DataNavigateUrlFields="ProjectID,ID" 
                    DataNavigateUrlFormatString="DocumentDetails.aspx?ProjectID={0}&amp;DocID={1}" 
                    Text="Details" >
                <ItemStyle Width="5%" />
                </asp:HyperLinkField>
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    
    </div>
    <hr />
    </form>
</body>
</html>
