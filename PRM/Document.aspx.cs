﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DMD;

namespace PRM
{  
    public partial class Data : System.Web.UI.Page
    {
        static Project oProject;

        protected void Page_Load(object sender, EventArgs e)
        {
            var sProjectID = Request.QueryString["ProjectID"];

            try
            {
                oProject = new Project(Convert.ToInt32(sProjectID));

                //Populate the datatable
                gvDocs.DataSource = oProject.Documents.Items;
                gvDocs.DataBind(); 
            }
            catch (Exception oException)
            {
                Response.Clear();
                Response.Write("<h1>" + oException.Message + "</h1>");                
                Server.ClearError();
            }           
        }      
    }
}