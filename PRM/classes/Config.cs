﻿using System;
using System.IO;
using System.Web;

namespace PRM
{
    public class Config
    {
        public string SERVICE_PATH { get; private set; }
        public Config()
        {            
            string text = File.ReadAllText(HttpRuntime.AppDomainAppPath + "/config.js");
            var test = new Jint.Engine();
            var result = test.Execute(text);

            SERVICE_PATH = result.GetValue("SERVICE_PATH").AsString();
                      
            //SERVICE_PATH = "http://localhost:65254/";
            if (!SERVICE_PATH.StartsWith("http"))
            {
                SERVICE_PATH = GetServerRoot() + SERVICE_PATH;
            }
        }


        private string GetServerRoot()
        {
            HttpRequest request = System.Web.HttpContext.Current.Request;

            string current = request.Url.ToString();
            string applicationPath = request.ApplicationPath;
            int applicationPathIndex = current.IndexOf(applicationPath, 10, StringComparison.InvariantCultureIgnoreCase);

            // should not be possible
            if (applicationPathIndex == -1)
            {
                throw new InvalidOperationException("Unable to derive root path");
            }

            string basePath = current.Substring(0, applicationPathIndex);
            return basePath;
        }
    }
}