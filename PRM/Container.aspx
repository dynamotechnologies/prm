﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Container.aspx.cs" Inherits="PRM.Container" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    
    <asp:TreeView ID="tvContainers" Header="{Binding Path=Attribute[order].Value}" 
        runat="server" Font-Names="Verdana" Font-Size="Small">
    </asp:TreeView>
    </form>
</body>
</html>
