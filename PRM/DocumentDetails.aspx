﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentDetails.aspx.cs" Inherits="PRM.DocumentDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 465px;
        }
        .style2
        {
            width: 139px;
        }
        .style3
        {
            width: 465px;
            height: 36px;
        }
        .style4
        {
            width: 133px;
        }
        .style6
        {
            width: 5px;
            height: 36px;
        }
        .style7
        {
            width: 5px;
        }
    </style>
   </head>
<body>
    <form id="form1" runat="server">
    
        <table style="width:100%; height: 400px;">
            <tr>
                <td class="style2" colspan="1">
    
        <asp:Label ID="lblGeneralInformation" runat="server" Text="General  " Font-Names="Verdana" 
                        Font-Bold="True" ForeColor="#3366FF"></asp:Label>
                </td>
                <td class="style1">
                    &nbsp;</td>
                <td class="style7">
                    &nbsp;</td>
                <td class="style4">
    
        <asp:Label ID="lblMetaDataInformation" runat="server" Text="Metadata" Font-Names="Verdana" 
                        Font-Bold="True" ForeColor="#3366FF"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style2">
    
        <asp:Label ID="lblDocID" runat="server" Text="Doc ID:  " Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style1">
    
                    <asp:TextBox ID="txtDocID" runat="server" Width="450px" Enabled="False" 
                        Font-Names="Verdana" Font-Size="Large" ReadOnly="True"></asp:TextBox>
                </td>
                <td class="style7">
    
                    &nbsp;</td>
                <td class="style4">
    
        <asp:Label ID="lblFileName" runat="server" Text="File Name:  " Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style9">
    
        <asp:Label ID="lblFileNameValue" runat="server" Font-Names="Verdana"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style2">
    
        <asp:Label ID="lblProjectIDGeneral" runat="server" Text="Project ID:" Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style1">
    
                    <asp:TextBox ID="txtProjectID" runat="server" width="450px" Enabled="False" 
                        Font-Names="Verdana" Font-Size="Large" ReadOnly="True"></asp:TextBox>
                </td>
                <td class="style7">
    
                    &nbsp;</td>
                <td class="style4">
    
        <asp:Label ID="lblFileSize" runat="server" Text="File Size:   " Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style9">
    
        <asp:Label ID="lblFileSizeValue" runat="server" Font-Names="Verdana"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style2">
    
        <asp:Label ID="lblProjectType" runat="server" Text="Project Type: " Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style1">
    
                    <asp:TextBox ID="txtProjectType" runat="server" width="450px" Enabled="False" 
                        Font-Names="Verdana" Font-Size="Large" ReadOnly="True"></asp:TextBox>
                </td>
                <td class="style7">
    
                    &nbsp;</td>
                <td class="style4">
    
        <asp:Label ID="lblTitle" runat="server" Text="Title:  " Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style9">
    
        <asp:Label ID="lblTitleValue" runat="server" Font-Names="Verdana"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style2">
    
        <asp:Label ID="lblPubFlag" runat="server" Text="Pub Flag:  " Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style3">
    
                    <asp:CheckBox ID="chkPubFlag" runat="server" />
                </td>
                <td class="style6">
    
                    &nbsp;</td>
                <td class="style4">
    
        <asp:Label ID="lblAuthor" runat="server" Text="Author:  " Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style9">
    
        <asp:Label ID="lblAuthorValue" runat="server" Font-Names="Verdana"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style2">
    
        <asp:Label ID="lblLink" runat="server" Text="Link: " Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style1">
    
                    <asp:TextBox ID="txtLink" runat="server" width="450px" Font-Names="Verdana" 
                        Font-Size="Large" Enabled="False" ReadOnly="True"></asp:TextBox>
                </td>
                <td class="style7">
    
                    &nbsp;</td>
                <td class="style4">
    
        <asp:Label ID="lblPubDate" runat="server" Text="Pub Date: " Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style9">
    
        <asp:Label ID="lblPubDateValue" runat="server" Font-Names="Verdana"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style2">
    
        <asp:Label ID="lblDocName" runat="server" Text="Doc Name:" Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style1">
    
                    <asp:TextBox ID="txtDocName" runat="server" width="450px" Font-Names="Verdana" 
                        Font-Size="Large"></asp:TextBox>
                </td>
                <td class="style7">
    
                    &nbsp;</td>
                <td class="style4">
    
        <asp:Label ID="lblAppSysID" runat="server" Text="App Sys ID:" Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style9">
    
        <asp:Label ID="lblAppSysIDValue" runat="server" Font-Names="Verdana"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style2">
    
        <asp:Label ID="lblDescription" runat="server" Text="Description:" Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style1">
    
                    <asp:TextBox ID="txtDescription" runat="server" width="450px" 
                        Font-Names="Verdana" Font-Size="Large"></asp:TextBox>
                </td>
                <td class="style7">
    
                    &nbsp;</td>
                <td class="style4">
    
        <asp:Label ID="lblAppDocType" runat="server" Text="App Doc Type:" Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style16">
    
        <asp:Label ID="lblAppDocTypeValue" runat="server" Font-Names="Verdana"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style2">
    
        <asp:Label ID="lblPdfFileSize" runat="server" Text="PDF File Size:" Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style1">
    
                    <asp:TextBox ID="txtPdfFileSize" runat="server" width="450px" 
                        Font-Names="Verdana" Font-Size="Large" Enabled="False" ReadOnly="True"></asp:TextBox>
                </td>
                <td class="style7">
    
                    &nbsp;</td>
                <td class="style4">
    
        <asp:Label ID="lblProjectID" runat="server" Text="Project ID:" Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style9">
    
        <asp:Label ID="lblProjectIDValue" runat="server" Font-Names="Verdana"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style2">
    
        <asp:Label ID="lblSensitiveFlag" runat="server" Text="Sensitive Flag:" Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style1">
    
                    <asp:CheckBox ID="chkSensitiveFlag" runat="server" Enabled="False" />
                </td>
                <td class="style7">
    
                    &nbsp;</td>
                <td class="style4">
    
        <asp:Label ID="lblDocDate" runat="server" Text="Doc Date:" Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style9">
    
                    <asp:TextBox ID="txtDocDate" runat="server" Width="300px" 
                        Font-Names="Verdana" Font-Size="Large"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
    
                    &nbsp;</td>
                <td class="style1">
    
                    &nbsp;</td>
                <td class="style7">
    
                    &nbsp;</td>
                <td class="style4">
    
        <asp:Label ID="lblDateLabel" runat="server" Text="Date Label:" Font-Names="Verdana"></asp:Label>
                </td>
                <td class="style9">
    
                    <asp:TextBox ID="txtDateLabel" runat="server" Width="300px" 
                        Font-Names="Verdana" Font-Size="Large"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
    
                    &nbsp;</td>
                <td class="style1">
    
                    &nbsp;</td>
                <td class="style7">
    
                    &nbsp;</td>
                <td class="style4">
    
                    &nbsp;</td>
                <td class="style9">
    
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" Width="88px" 
                        onclick="btnUpdate_Click" />
                </td>
            </tr>
        </table>
    <div>
    
    </div>
    </form>
</body>
</html>
