﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace DMDService
{
	public class Token
	{

		public string pid;
		public string uid;
		public Token()
		{

		}

		public Token(string token)
		{
			uid = null;
			pid = null;
		}
		public Token (string project_id, string pals_pw, string userid)
		{
			uid = userid;
			pid = project_id;
		}

		public void SaveToken (string token)
		{
			TokenInfo tokenInfo = new TokenInfo(token, uid, pid);
			HttpRuntime.Cache.Add(tokenInfo.TokenString, tokenInfo, null, DateTime.UtcNow.AddMinutes(10.0), Cache.NoSlidingExpiration,
				CacheItemPriority.Normal, null);
			
		}
		    
		public Guid GenerateToken ()
		{
			Guid token = Guid.NewGuid();
			SaveToken(token.ToString());
			return token;
		}

		public string GetTokenInfo(string tokenString)
		{
			
			var tokenInfoCache = (TokenInfo)HttpRuntime.Cache.Get(tokenString);
			return tokenInfoCache.Pid + "," + tokenInfoCache.Uid;
		}
	}
}