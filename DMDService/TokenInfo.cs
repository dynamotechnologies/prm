﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMDService
{
	public class TokenInfo
	{
		public string Pid;
		public string Uid;
		public string TokenString;
		public TokenInfo()
		{ }

		public TokenInfo(string token)
		{
			Pid = null;
			Uid = null;
			TokenString = token;
		}
		public TokenInfo(string token, string userId, string projectId)
		{
			Pid = projectId;
			Uid = userId;
			TokenString = token;
		}

		public override string ToString()
		{
			return Pid + "," + Uid;
		}

		
	}
}