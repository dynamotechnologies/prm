using DMD;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel.Web;
using System.Text;

namespace DMDService
{
    public class Main : IMain
    {
        // Create a delegate instance 
        delegate string ServiceMethodClosure();

        static Main()
        {
            Configuration.init(Properties.Settings.Default);
        }

        #region "Service Methods"

        public string GetProjectInfo(string token)
        {
            //"Not implemented" is a wierd error status to throw instead of internal error.  But the original
            //function did this and I'm not inclined to change it at this point
            string log = getLogString(MethodBase.GetCurrentMethod(), token);
            return InvokeServiceMethod(log, HttpStatusCode.NotImplemented, (ServiceMethodClosure)delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Read);

                //in addition to the project we send some information about the user to limit the number
                //of ajax calls needed to render the page. 
                Project oProject = new Project(Convert.ToInt32(oUserSession.ProjectID), ProjectInfo.Header);
                var data = new
                {
                    project = oProject,
                    userinfo = new
                    {
                        userid = oUserSession.UserName,
                        canedit = oUserSession.hasPermission(UserSession.Permission.Edit)
                    }
                };
                return JsonConvert.SerializeObject(data, Formatting.None, new IsoDateTimeConverter() { DateTimeFormat = "MM/dd/yyyy" });
            });
        }

        public string GetProject(string token, string loadview)
        {
            //"Not implemented" is a wierd error status to throw instead of internal error.  But the original
            //function did this and I'm not inclined to change it at this point
            string log = getLogString(MethodBase.GetCurrentMethod(), token);
            return InvokeServiceMethod(log, HttpStatusCode.NotImplemented, delegate()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Read);
                Project.LoadView = loadview;

                Project oProject = new Project(Convert.ToInt32(oUserSession.ProjectID), loadview);
               
                var oNodes = GetContainerItems(oProject.Containers.Items);


                if (oProject.Containers.Documents.Count > 0)
                {
                    oNodes.AddRange(oProject.Containers.Documents.Select(oDocument => new NodeProxy
                    {
                        id = oDocument.ID,
                        text = oDocument.Name,
                        type = "file"
                    }));
                }

                return JsonConvert.SerializeObject(oNodes);
            });
        }

        public string GetProjectDocuments(string token, string parent_node_id)
        {
            //"Not implemented" is a wierd error status to throw instead of internal error.  But the original
            //function did this and I'm not inclined to change it at this point
            string log = getLogString(MethodBase.GetCurrentMethod(), token, parent_node_id);
            return InvokeServiceMethod(log, HttpStatusCode.NotImplemented, delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Read);

                Project oProject = new Project(Convert.ToInt32(oUserSession.ProjectID));
                DocumentsProxy oDocumentsProxy = new DocumentsProxy(oUserSession.hasPermission(UserSession.Permission.Edit));
                List<Document> oDocuments;

                if (string.IsNullOrEmpty(parent_node_id))
                {
                    oDocuments = oProject.Documents.Items;
                }
                else
                {
                    int iParentID;
                    if (Int32.TryParse(parent_node_id, out iParentID))
                    {
                        oDocuments = oProject.Documents.GetDocumentsByParentID(iParentID);
                        oDocuments.Sort();
                    }
                    else
                    {
                        throw new Exception("Invalid Forder ID.");
                    }
                }

                if (oDocuments.Count > 0)
                {
                    foreach (Document oDocument in oDocuments)
                    {
                        oDocumentsProxy.data.Add(new DocumentProxy(oDocument));
                    }
                }

                return JsonConvert.SerializeObject(
                    oDocumentsProxy, 
                    Formatting.None, 
                    new IsoDateTimeConverter() { DateTimeFormat = "MM/dd/yyyy" }
                );
            });
        }

        public string MoveFolder(string token, string node_id, string parent_node_id, string target_node_id, string after_id)
        {
            //"Not implemented" is a wierd error status to throw instead of internal error.  But the original
            //function did this and I'm not inclined to change it at this point
            string log = getLogString(MethodBase.GetCurrentMethod(), token, node_id, parent_node_id, target_node_id);
            return InvokeServiceMethod(log, HttpStatusCode.NotImplemented, delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Edit);

                Project oProject = new Project(Convert.ToInt32(oUserSession.ProjectID));
                oProject.Containers.Move(node_id, parent_node_id, target_node_id, after_id);
                return "";
            });
        }

        public string AddFolder(string token, string parent_node_id, string text)
        {
            //"Not implemented" is a wierd error status to throw instead of internal error.  But the original
            //function did this and I'm not inclined to change it at this point
            string log = getLogString(MethodBase.GetCurrentMethod(), token, parent_node_id, text);
            return InvokeServiceMethod(log, HttpStatusCode.NotImplemented, delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Edit);

                long iNewNodeID;
                Project oProject = new Project(Convert.ToInt32(oUserSession.ProjectID));
                oProject.Containers.Add(parent_node_id, text, false, out iNewNodeID);
                String NodeID = iNewNodeID.ToString();

                return JsonConvert.SerializeObject(NodeID);
            });
        }

        public string RemoveFolder(string token, string node_id)
        {
            //"Not implemented" is a wierd error status to throw instead of internal error.  But the original
            //function did this and I'm not inclined to change it at this point
            string log = getLogString(MethodBase.GetCurrentMethod(), token, node_id);
            return InvokeServiceMethod(log, HttpStatusCode.NotImplemented, delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Edit);

                Project oProject = new Project(Convert.ToInt32(oUserSession.ProjectID));
                oProject.Containers.Remove(node_id);
                return "";
            });
        }

        public string UpdateFolder(string token, string node_id, string text, string static_flag)
        {
            //"Not implemented" is a wierd error status to throw instead of internal error.  But the original
            //function did this and I'm not inclined to change it at this point
            string log = getLogString(MethodBase.GetCurrentMethod(), token, node_id, static_flag);
            return InvokeServiceMethod(log, HttpStatusCode.NotImplemented, delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Edit);

                //the text comes to us html encoded.  This was being implicitly unencoded when passed back 
                //to the Datamart prior to fixing the XML.  This restores the behavior of not saving the 
                //text as HTML escaped in the database.
                //Need to sort out who needs to handle what where in terms of encoding.
                //Because the front end should be passing us the data like we plan to save it and 
                //we shouldn't be doing this.
                text = WebUtility.HtmlDecode(text);

                Project oProject = new Project(Convert.ToInt32(oUserSession.ProjectID));
                oProject.Containers.Update(node_id, text, static_flag == "1");
                return "";
            });
        }

        public string UpdateDocument(
            string token,
            string doc_id,
            string public_flag,
            string name,
            string description,
            string sensitivity,
            string author,
            string addressee,
            string datelabel,
            string date
        )
        {
            //"Not implemented" is a wierd error status to throw instead of internal error.  But the original
            //function did this and I'm not inclined to change it at this point
            string log = getLogString(MethodBase.GetCurrentMethod(), token, doc_id, public_flag, name, description,
                sensitivity, author, addressee, datelabel, date);
            return InvokeServiceMethod(log, HttpStatusCode.NotImplemented, delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Edit);

                Project oProject = new Project(Convert.ToInt32(oUserSession.ProjectID), ProjectInfo.Header);
                Document oDocument = new Document(doc_id, oUserSession.ProjectID, oProject.ProjectDocumentID)
                {
                    Public = public_flag == "1",
                    Name = name,
                    Description = description,
                    SensitivityRationale = Convert.ToInt16(sensitivity),
                    Author = author,
                    Addressee = addressee,
                    DateLabel = datelabel
                };

                DateTime dDocumentDate;
                if (DateTime.TryParse(date, out dDocumentDate))
                {
                    oDocument.Date = dDocumentDate;
                }
                else
                {
                    oDocument.Date = null;
                }

                oDocument.Update();

                return "Success";
            });
        }

        //Added to update each field in UCM
        public string UpdateFieldDocument(
        string token,
        string doc_id,
        string public_flag,
        string name,
        string description,
        string sensitivity,
        string author,
        string addressee,
        string datelabel,
        string date,
        string currentrow,
        string currentcolumn,
        string previousvalue,
        string currentvalue
    )
        {
            //"Not implemented" is a wierd error status to throw instead of internal error.  But the original
            //function did this and I'm not inclined to change it at this point
            string log = getLogString(MethodBase.GetCurrentMethod(), token, doc_id, public_flag, name, description,
                sensitivity, author, addressee, datelabel, date, currentrow, currentcolumn, previousvalue, currentvalue);
            return InvokeServiceMethod(log, HttpStatusCode.NotImplemented, delegate()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Edit);

                Project oProject = new Project(Convert.ToInt32(oUserSession.ProjectID), ProjectInfo.Header);
                Document oDocument = new Document(doc_id, oUserSession.ProjectID, oProject.ProjectDocumentID)
                {
                    Public = public_flag == "1",
                    Name = name,
                    Description = description,
                    SensitivityRationale = Convert.ToInt16(sensitivity),
                    Author = author,
                    Addressee = addressee,
                    DateLabel = datelabel,
                    Currentrow = currentrow,
                    Currentcolumn = currentcolumn,
                    Previousvalue = previousvalue,
                    Currentvalue = currentvalue 
                };

                DateTime dDocumentDate;
                if (DateTime.TryParse(date, out dDocumentDate))
                {
                    oDocument.Date = dDocumentDate;
                }
                else
                {
                    oDocument.Date = null;
                }

                oDocument.UpdateField();

                return "Success";
            });
        }

        public string GetDocumentDetails(string token, string doc_id)
        {
            //"Not implemented" is a wierd error status to throw instead of internal error.  But the original
            //function did this and I'm not inclined to change it at this point
            string log = getLogString(MethodBase.GetCurrentMethod(), token, doc_id);
            return InvokeServiceMethod(log, HttpStatusCode.NotImplemented, delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Read);

                Document oDocument = new Document(doc_id, oUserSession.ProjectID);
                return JsonConvert.SerializeObject(oDocument);
            });
        }

        public Stream DownloadProjectDocument(string token, string doc_id, string pdf_flag)
        {
            string sReply;
            bool bError = false;
            string sFileName = "";
            Stream oReply = null;

            try
            {

                OutgoingWebResponseContext outResponse = WebOperationContext.Current.OutgoingResponse;
                outResponse.Headers.Add("Cache-Control", "no-cache");
                outResponse.Headers.Add("Pragma", "no-cache");
                outResponse.Headers.Add("Expires", "0");

                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Read);

                Document oDocument = new Document(doc_id, oUserSession.ProjectID);
                byte[] oContent = oDocument.GetContent(pdf_flag == "1");

                if (oContent.Length == 1)
                {
                    string emessage = "Document is not yet ready. Please try to publish after sometime";

                    throw new Exception(emessage);
                }

                if (pdf_flag != "1")
                {
                    sFileName = oDocument.FileMetaData.filename;
                }
                else
                {
                    string[] oFileParts = oDocument.FileMetaData.filename.Split('.');
                    if (oFileParts.Count() > 0)
                    {
                        sFileName = oFileParts[0];
                    }
                    else
                    {
                        sFileName = "DMDFile";
                    }
                    sFileName = sFileName + ".pdf";
                }

                outResponse.Headers.Add("Content-Disposition", string.Format("attachment; filename={0}", sFileName));

                oReply = new MemoryStream(oContent);
                sReply = "Downloaded file " + sFileName;
            }
            catch (Exception e)
            {
                sReply = e.Message;
                bError = true;
                OutgoingWebResponseContext outResponse = WebOperationContext.Current.OutgoingResponse;
                outResponse.StatusCode = System.Net.HttpStatusCode.NotImplemented;
                outResponse.StatusDescription = sReply;
                throw;
            }

            General.WriteToEventLog("OpenProjectDocument (token: " + token + ", doc_id: " + doc_id + "). Reply: " + sReply, bError);
            return oReply;
        }

        public string ReOrderDocuments(string token, string doc_id, string position)
        {
            //"Not implemented" is a wierd error status to throw instead of internal error.  But the original
            //function did this and I'm not inclined to change it at this point
            string log = getLogString(MethodBase.GetCurrentMethod(), token, doc_id, position);
            return InvokeServiceMethod(log, HttpStatusCode.NotImplemented, delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Edit);

                Project oProject = new Project(Convert.ToInt32(oUserSession.ProjectID));
                oProject.Documents.ReOrder(doc_id, Convert.ToInt32(position));
                return "";
            });
        }

        public string ReOrderAllDocuments(string token, string node_id, string sort)
        {
            //"Not implemented" is a wierd error status to throw instead of internal error.  But the original
            //function did this and I'm not inclined to change it at this point
            string log = getLogString(MethodBase.GetCurrentMethod(), token, node_id, sort);
            return InvokeServiceMethod(log, HttpStatusCode.NotImplemented, delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Edit);

                Project oProject = new Project(Convert.ToInt32(oUserSession.ProjectID));
                int iSort = Convert.ToInt32(sort);
                oProject.Documents.ReOrderAll(Convert.ToInt32(node_id), iSort == 1 ? DocumentSortOrder.Alphabetical : DocumentSortOrder.DateAdded);
                return "";
            });
        }

        public string GetToken(string project_id, string pals_pw, string userid, string accessLevel)
        {
            string log = getLogString(MethodBase.GetCurrentMethod(), project_id, pals_pw, userid);
            return InvokeServiceMethod(log, HttpStatusCode.BadRequest, delegate ()
            {
                // We have three modes we want to support 
                // 1) No password is required (token generation is wide open) for dev/test environments
                // 2) No token generation is allowed (if we move token generation to DM for production)
                // 3) A specific password needs to be passed -- to tighten up production and to allow 
                //      debug access on live as needed.
                String expectedPassword = Configuration.TokenPassword;
                if (expectedPassword != "*any*" && (expectedPassword == "" || expectedPassword != pals_pw))
                {
                   throw new Exception("Invalid password sent to token generation");
                }

                //TODO: 1) Retrieve Project.UnitCode, User.UnitRoles
                //TODO: 2) Verify authorization
                //TODO: 3) Create UserSession in database, session
                //TODO: 4) Convert URLS to include token instead of raw parameters
                //TODO: 5) Create web filter to retrieve parameters from stored token
                //TODO: 6) Determine business logic for session expiration and apply to web filter

                UserSession oUserSession = new UserSession(userid, project_id, accessLevel);
                return oUserSession.Token;
            });
        }

        public string GetSessionInfo(string token)
        {
            string log = getLogString(MethodBase.GetCurrentMethod(), token);
            return InvokeServiceMethod(log, delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Read);
                var info = new
                {
                    projectid = oUserSession.ProjectID,
                    username = oUserSession.UserName,
                    canedit = oUserSession.hasPermission(UserSession.Permission.Edit)
                };

                return JsonConvert.SerializeObject(info);
            });
        }

        public string SaveTemplate(string token, string name, string unitid)
        {
            string log = getLogString(MethodBase.GetCurrentMethod(), token, name, unitid);
            return InvokeServiceMethod(log, delegate ()
            {
                if (name == null || name.Trim().Equals(""))
                {
                    throw new Exception("Name is required");
                }

                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Edit);

                Project project = new Project(Convert.ToInt32(oUserSession.ProjectID));
                Template.CreateFromProject(project, name, unitid).Commit();
                return "";
            });
        }

        public string DeleteTemplate(string token, int templateid)
        {
            string log = getLogString(MethodBase.GetCurrentMethod(), token, templateid);
            return InvokeServiceMethod(log, delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Edit);

                Template.Delete(templateid);
                return "";
            });
        }

        public string GetTemplateHeaders(string token, int page, int pagesize)
        {
            string log = getLogString(MethodBase.GetCurrentMethod(), page, pagesize);
            return InvokeServiceMethod(log, delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Read);

                //allow multiple copies of "unitid" or no value at all.
                string[] unitids = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters.GetValues("unitid");

                TemplateHeaderPage headers;
                if (unitids == null || unitids.Length == 0)
                {
                    headers = TemplateHeaderPage.load(page, pagesize);
                }
                else
                {
                    headers = TemplateHeaderPage.load(unitids, page, pagesize);
                }
                return JsonConvert.SerializeObject(headers);
            });
        }

        public string GetUnitHeaders(string token, string sort)
        {
            string log = getLogString(MethodBase.GetCurrentMethod(), token);
            return InvokeServiceMethod(log, delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Read);

                List<UnitHeader> headers = UnitHeader.getHeaders();
                switch(sort)
                {
                    case "unitid":
                        headers.Sort((x, y) => x.unitid.CompareTo(y.unitid));
                        break;
                    default:
                        headers.Sort((x, y) => x.name.CompareTo(y.name));
                        break;
                }

                return JsonConvert.SerializeObject(headers);
            });
        }

        public string ApplyTemplate(string token, int templateid)
        {
            string log = getLogString(MethodBase.GetCurrentMethod(), token, templateid);
            return InvokeServiceMethod(log, delegate ()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Edit);

                Template template = Template.Load(templateid);
                Project project = new Project(Convert.ToInt32(oUserSession.ProjectID));
                project.applyTemplate(template);
                return "";
            });
        }
        
        public string GetViews(string token)
        {
            string log = getLogString(MethodBase.GetCurrentMethod(), token);
            return InvokeServiceMethod(log, delegate()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Read);

                Project projectid = new Project(Convert.ToInt32(oUserSession.ProjectID));
                string userid = oUserSession.UserName;

                List<ViewsHeader> headers = ViewsHeader.getViewHeaders(projectid, userid);

                return JsonConvert.SerializeObject(headers);
            });
        }
        public string SaveView(string token, string name)
        {
            string log = getLogString(MethodBase.GetCurrentMethod(), token, name);
            return InvokeServiceMethod(log, delegate()
            {
                if (name == null || name.Trim().Equals(""))
                {
                    throw new Exception("Name is required");
                }

                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Edit);

                Project project = new Project(Convert.ToInt32(oUserSession.ProjectID));
                string userid = oUserSession.UserName;

                ViewsHeader.createView(project, userid, name);
                return "";
            });
        }
        public string AddToView(string token, string node_id, string viewid)
        {
            string log = getLogString(MethodBase.GetCurrentMethod(), token, node_id, viewid);
            return InvokeServiceMethod(log, delegate()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Edit);

                Project project = new Project(Convert.ToInt32(oUserSession.ProjectID));

                ViewsHeader.addToView(project, node_id, viewid);
                return "";
            });
        }

        public string DeleteView(string token, string viewid)

        {
            string log = getLogString(MethodBase.GetCurrentMethod(), token, viewid);
            return InvokeServiceMethod(log, delegate()
            {
                UserSession oUserSession = new UserSession(token);
                oUserSession.checkPermission(UserSession.Permission.Edit);

                Project project = new Project(Convert.ToInt32(oUserSession.ProjectID));

                ViewsHeader.deleteView(project,viewid);
                return "";
            });
        }

        #endregion

        #region "Private Methods"

        private string getLogString(MethodBase caller, params Object[] callparams)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(caller.Name).Append("(");
            foreach (ParameterInfo p in caller.GetParameters())
            {
                sb.Append(p.Name).Append(": ");
                try
                {
                    sb.Append(callparams[p.Position]);
                }
                catch (IndexOutOfRangeException)
                {
                    sb.Append("value not provided");
                }
                sb.Append(", ");
            }

            sb.Length -= (", ").Length;
            sb.Append(").");
            
            return sb.ToString();
        }
    

        private string InvokeServiceMethod(String log, ServiceMethodClosure instance)
        {
            return InvokeServiceMethod(log, HttpStatusCode.InternalServerError, instance);
        }
    
        private string InvokeServiceMethod(String log, HttpStatusCode errorstatus, ServiceMethodClosure instance)
        {
            string sReply = "";
            bool bError = false;
            OutgoingWebResponseContext outResponse = WebOperationContext.Current.OutgoingResponse;

            try
            {
                //I can't think of a reason we'd ever want to cache.  Even errors.  
                //Add this to the general case and do it before the actual code so that 
                //if we hit an exception we don't skip the no cache.
                //if somebody does want to allow caching we'll figure out how to skip this
                //when it happens.
                outResponse.Headers.Add("Cache-Control", "no-cache");
                outResponse.Headers.Add("Pragma", "no-cache");
                outResponse.Headers.Add("Expires", "0");

                //actually do what we came here to do.
                sReply = instance.Invoke();
            }
            catch (WebException e)
            {
                string responseText = "";
                if (e.Response != null)
                {
                    //stream reader will dispose of the stream automagically, do not 
                    //need a seperate using for the stream.
                    var responseStream = e.Response.GetResponseStream();
                    if (responseStream != null)
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseText = reader.ReadToEnd();
                        }
                    }
                }

                sReply = responseText;
                bError = true;
                outResponse.StatusCode = errorstatus;
                outResponse.StatusDescription = e.Message;
            }
            catch (Exception e)
            {
                sReply = e.Message;
                bError = true;
                outResponse.StatusCode = errorstatus;
                outResponse.StatusDescription = e.Message;
            }

        
                //if the reply is big, don't log it.
                if (sReply.Length < 10240)
                {
                    General.WriteToEventLog(log + " Reply: " + sReply, bError);
                }
                else
                {
                    General.WriteToEventLog(log + " Reply: too large to log", bError);
                }
            
            return sReply;
        }
   

        private List<NodeProxy> GetContainerItems(List<Container> Containers)
        {
            List<NodeProxy> oNodes = new List<NodeProxy>();

            foreach (Container oContainer in Containers)
            {
                NodeProxy oNode = new NodeProxy();
                oNode.id = oContainer.ID.ToString();
                oNode.text = oContainer.Label;
                oNode.data.stationary = oContainer.Static;
                oNode.type = "default";

                if (oContainer.Containers.Count > 0)
                {
                    oNode.children = GetContainerItems(oContainer.Containers);
                }

                if (oContainer.Documents.Count > 0)
                {
                    foreach (Document oDocument in oContainer.Documents)
                    {
                        NodeProxy oDocumentNode = new NodeProxy();
                        oDocumentNode.id = oDocument.ID.ToString();
                        oDocumentNode.text = oDocument.Name;
                        oDocumentNode.data.stationary = false;
                        oDocumentNode.type = "file";
                        oNode.children.Add(oDocumentNode);
                    }
                }
                oNodes.Add(oNode);
            }

            return oNodes;
        }

        #endregion
    }

    #region "Service Proxies"

    public struct NodeState
    {
        public bool opened;
        public bool disabled;
        public bool selected;
    }

    public class NodeData
    {
        public bool stationary; 
    }

    public class NodeProxy
    {
	    private NodeState _state;

	    public string id { get; set; }

	    public string text { get; set; }

	    public string type { get; set; }

	    public NodeData data { get; set; }

	    public NodeState state
        {
            get { return _state; }
        }

        public List<NodeProxy> children { get; set; }

	    public NodeProxy()
        {
            children = new List<NodeProxy>();
            _state.opened = false;
            _state.disabled = false;
            _state.selected = false;
		    data = new NodeData {stationary = false};
        }
    }

    public class DocumentProxy
    {
        static int idCount = 0;

	    public int id { get; set; }

	    public Document values { get; set; }

	    public DocumentProxy()
        {
            idCount++;
            id = idCount;
            values = new Document();
        }

        public DocumentProxy(Document values)
        {
            idCount++;
            id = idCount;
            this.values = values;
        }
    }

   
    public class DocumentMetaData
    {
	    public string name { get; set; }

	    public string label { get; set; }

	    public string datatype { get; set; }

	    public bool editable { get; set; }

        public Dictionary<int, String> values { get; set; }

        public DocumentMetaData(string name, string label, string datatype, bool editable, Dictionary<int, String> values = null)
        {
            this.name = name;
            this.label = label;
            this.datatype = datatype;
            this.editable = editable;
            this.values = values;
        }
    }

    public class DocumentsProxy
    {
	    public List<DocumentMetaData> metadata { get; set; }

	    public List<DocumentProxy> data { get; set; }
        //Added to delete a row in editable grid
	    public DocumentsProxy(bool canedit)
        {
            metadata = new List<DocumentMetaData>();
            data = new List<DocumentProxy>();

            metadata.Add(new DocumentMetaData("ID", "DocID", "string", false));
            metadata.Add(new DocumentMetaData("AllowPublish", "Allow Publish/Unpublish action", "boolean", false));
            metadata.Add(new DocumentMetaData("deleteRow", "Delete", "action", false));
            metadata.Add(new DocumentMetaData("Public", "Publish to Web?", "boolean", canedit));
            metadata.Add(new DocumentMetaData("Name", "Name", "string", canedit));
            metadata.Add(new DocumentMetaData("Description", "Description", "string", canedit));            
            metadata.Add(new DocumentMetaData("Author", "Author", "string", canedit));
            metadata.Add(new DocumentMetaData("Addressee", "Addressee", "string", canedit));
            metadata.Add(new DocumentMetaData("DateLabel", "Date Type", "string", canedit, General.GetDateLabels()));
            metadata.Add(new DocumentMetaData("Date", "Date", "date", canedit));
			metadata.Add(new DocumentMetaData("DateAdded", "Date Added", "date", false));
			metadata.Add(new DocumentMetaData("AddedBy", "Added By", "string", false));
            metadata.Add(new DocumentMetaData("SensitivityRationale", "Sensitivity Rationale", "string", canedit, General.GetSensitivityRationales()));
            metadata.Add(new DocumentMetaData("DownloadOriginal", "Download Original", "website", false));
			metadata.Add(new DocumentMetaData("DownloadPDF", "Download PDF", "website", false));
			metadata.Add(new DocumentMetaData("PDFfileSize", "Size, (kb)", "integer", false));
            metadata.Add(new DocumentMetaData("ContID", "ContID", "string", false));
            
        }

    }
    #endregion 
}
