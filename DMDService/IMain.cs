﻿using System.ServiceModel;
using System.ServiceModel.Web;
using System.IO;

namespace DMDService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMain" in both code and config file together.
    [ServiceContract]
    public interface IMain
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "project/getinfo?token={token}")]
        string GetProjectInfo(string token);

        [OperationContract]
        [WebInvoke(Method = "GET", 
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "project/get?token={token}&loadview={loadview}")]
        string GetProject(string token, string loadview);       

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "folder/move?token={token}&node_id={node_id}&parent_node_id={parent_node_id}&target_node_id={target_node_id}&after_id={after_id}")]
        string MoveFolder(string token, string node_id, string parent_node_id, string target_node_id, string after_id);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "folder/add?token={token}&parent_node_id={parent_node_id}&text={text}")]
        string AddFolder(string token, string parent_node_id, string text);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "folder/remove?token={token}&node_id={node_id}")]
        string RemoveFolder(string token, string node_id);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "folder/update?token={token}&node_id={node_id}&text={text}&static_flag={static_flag}")]
        string UpdateFolder(string token, string node_id, string text, string static_flag);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "documents/get?token={token}&parent_node_id={parent_node_id}")]
        string GetProjectDocuments(string token, string parent_node_id);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "documents/update?token={token}&doc_id={doc_id}&public_flag={public_flag}&name={name}&description={description}&sensitivity={sensitivity}&author={author}&addressee={addressee}&datelabel={datelabel}&date={date}")]
        string UpdateDocument(string token, string doc_id, string public_flag, string name, string description, string sensitivity, string author, string addressee, string datelabel, string date);

        //Added to update each field in UCM
        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "documents/updatefield?token={token}&doc_id={doc_id}&public_flag={public_flag}&name={name}&description={description}&sensitivity={sensitivity}&author={author}&addressee={addressee}&datelabel={datelabel}&date={date}&currentrow={currentrow}&currentcolumn={currentcolumn}&previousvalue={previousvalue}&currentvalue={currentvalue}")]
        string UpdateFieldDocument(string token, string doc_id, string public_flag, string name, string description, string sensitivity, string author, string addressee, string datelabel, string date, string currentrow,string currentcolumn,string previousvalue, string currentvalue);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "documents/details?token={token}&doc_id={doc_id}")]
        string GetDocumentDetails(string token, string doc_id);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Xml,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "documents/download?token={token}&doc_id={doc_id}&pdf_flag={pdf_flag}")]
        Stream DownloadProjectDocument(string token, string doc_id, string pdf_flag);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "documents/reorder?token={token}&doc_id={doc_id}&position={position}")]
        string ReOrderDocuments(string token, string doc_id, string position);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "documents/reorderall?token={token}&node_id={node_id}&sort={sort}")]
        string ReOrderAllDocuments(string token, string node_id, string sort);

		[OperationContract]
		[WebInvoke(Method = "GET",
					ResponseFormat = WebMessageFormat.Xml,
					BodyStyle = WebMessageBodyStyle.Wrapped,
					UriTemplate = "authentication/gettoken?project_id={project_id}&pals_pw={pals_pw}&userid={userid}&accessLevel={accessLevel}")]
		string GetToken(string project_id, string pals_pw, string userid, string accessLevel);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "authentication/sessioninfo?token={token}")]
        string GetSessionInfo(string token);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "templates/save?token={token}&name={name}&unitid={unitid}")]
        string SaveTemplate(string token, string name, string unitid);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "templates/delete?token={token}&templateid={templateid}")]
        string DeleteTemplate(string token, int templateid);

        [OperationContract]
        [WebInvoke(Method = "GET",
             ResponseFormat = WebMessageFormat.Json,
             BodyStyle = WebMessageBodyStyle.Wrapped,
             UriTemplate = "templates/headers?token={token}&page={page}&pagesize={pagesize}")]
        //also allow unitid to be specified. This is option and multiple are allowed.
        //if not specified all headers will be returned.
        string GetTemplateHeaders(string token, int page, int pagesize);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "templates/apply?token={token}&templateid={templateid}")]
        string ApplyTemplate(string token, int templateid);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "units/headers?token={token}&sort={sort}")]
        string GetUnitHeaders(string token, string sort);
       
        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "views/names?token={token}")]
        string GetViews(string token);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "views/save?token={token}&name={name}")]
        string SaveView(string token, string name);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "views/add?token={token}&node_id={node_id}&viewid={viewid}")]
        string AddToView(string token, string node_id, string viewid);

        [OperationContract]
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    BodyStyle = WebMessageBodyStyle.Wrapped,
                    UriTemplate = "views/delete?token={token}&viewid={viewid}")]
        string DeleteView(string token, string viewid);
    }
}
